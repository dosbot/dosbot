package com.itwookie.logger;

import com.itwookie.utils.Listener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class Logger {

    private static class LineOutputStream extends OutputStream {
        private Consumer<String> lc;
        private StringBuilder sb = new StringBuilder(80);

        public LineOutputStream(Consumer<String> lineConsumer) {
            lc = lineConsumer;
        }

        @Override
        public void write(int b) {
            char c = (char) (b & 0xff);
            if (c == '\n') {
                lc.accept(sb.toString());
                sb.setLength(0);
            } else {
                sb.append((char) (b & 0xff));
            }
        }
    }

    private static class InputStreamPiper extends Thread {
        private BufferedReader reader;

        private InputStreamPiper() {
            reader = new BufferedReader(new InputStreamReader(systemIn));
            setName("Logger InputStream Pipe");
        }
        @Override
        public void run() {
            char[] buffer = new char[256];
            int r;
            try {
                for (; ; ) {
                    while (reader.ready() && (r = reader.read(buffer, 0, buffer.length)) > 0) {
                        inputStreamProxy.write(buffer, 0, r);
                        inputStreamProxy.flush();
                    }
                    Thread.sleep(50);
                }
            } catch (InterruptedException ignore) {
                /**/
            } catch (Exception e) {
                e.printStackTrace(systemErr);
            }
        }
    }

    private static PrintStream systemOut;
    private static PrintStream systemErr;
    private static InputStream systemIn;
    private static Map<String, Logger> cachedLogger = new HashMap<>();
    private static Logger defaultLogger;
    private static PipedInputStream pis;
    private static BufferedWriter inputStreamProxy;
    private static InputStreamPiper autoPipe;

    static void writeInput(String line) {
        try {
            inputStreamProxy.write(line.trim());
            inputStreamProxy.write('\n');
            inputStreamProxy.flush();
        } catch (Exception e) {
            e.printStackTrace(systemErr);
        }
    }

    static {
        defaultLogger = new Logger();
        cachedLogger.put("Default", defaultLogger);

        //wrap system out
        systemOut = System.out;
        PrintStream pso = new PrintStream(new LineOutputStream((line) -> defaultLogger.log(line)), true, StandardCharsets.UTF_8);
        System.setOut(pso);

        //wrap system error
        systemErr = System.err;
        PrintStream pse = new PrintStream(new LineOutputStream((line) -> defaultLogger.err(line)), true, StandardCharsets.UTF_8);
        System.setErr(pse);

        //wrap system in
        systemIn = System.in;
        try {
            pis = new PipedInputStream();
            PipedOutputStream pos = new PipedOutputStream(pis);
            inputStreamProxy = new BufferedWriter(new OutputStreamWriter(pos));
        } catch (Exception e) {
            e.printStackTrace(systemErr);
        }
        System.setIn(pis);
        autoPipe = new InputStreamPiper();
        autoPipe.start();
    }

    private String tag;

    private Logger() {
        this("Default");
    }

    private Logger(String tag) {
        this.tag = tag;
    }

    public static Logger getLogger() {
        return getLogger("Default");
    }

    public static Logger getLogger(String tag) {
        if (cachedLogger.containsKey(tag))
            return cachedLogger.get(tag);
        else {
            Logger logger = new Logger(tag);
            cachedLogger.put(tag, logger);
            return logger;
        }
    }

    /**
     * this is intended for short term use to log small specific things with increased visibility
     */
    public void yell(String string) {
        systemOut.println(String.format("[%s][YELL] %s", tag, string));
        if (loggerUI != null) loggerUI.yell(tag, string);
    }

    /**
     * this is intended for short term use to log small specific things with increased visibility
     */
    public void yell(@NotNull Object object1, Object... objects) {
        StringBuilder sb = new StringBuilder();
        sb.append(object1);
        for (Object further : objects) sb.append(further);
        yell(sb.toString());
    }

    public void log(String string) {
        systemOut.println(String.format("[%s][INFO] %s", tag, string));
        if (loggerUI != null) loggerUI.log(tag, string);
    }

    public void log(@NotNull Object object1, Object... objects) {
        StringBuilder sb = new StringBuilder();
        sb.append(object1);
        for (Object further : objects) sb.append(further);
        log(sb.toString());
    }

    public void warn(String string) {
        systemErr.println(String.format("[%s][WARN] %s", tag, string));
        if (loggerUI != null) loggerUI.warn(tag, string);
    }

    public void warn(@NotNull Object object1, Object... objects) {
        StringBuilder sb = new StringBuilder();
        sb.append(object1);
        for (Object further : objects) sb.append(further);
        warn(sb.toString());
    }

    public void err(String string) {
        systemErr.println(String.format("[%s][ERR ] %s", tag, string));
        if (loggerUI != null) loggerUI.err(tag, string);
    }

    public void err(@NotNull Object object1, Object... objects) {
        StringBuilder sb = new StringBuilder();
        sb.append(object1);
        for (Object further : objects) sb.append(further);
        err(sb.toString());
    }

    private static MultiTabLogger loggerUI = new MultiTabLogger();

    public static void showMultiTabLogger(@Nullable Listener closeListener) {
        if (!loggerUI.getFrame().isDisplayable()) {
            loggerUI.getFrame().dispose();
            loggerUI = new MultiTabLogger();
        }
        loggerUI.setCloseListener(closeListener);
        loggerUI.getFrame().setVisible(true);
        loggerUI.getFrame().setAlwaysOnTop(true);
        loggerUI.getFrame().setAlwaysOnTop(false);
    }

    public static void close() {
        autoPipe.interrupt();
        cachedLogger.clear();
        loggerUI.getFrame().dispose();
        loggerUI = null;

        //revert streams in case this was not the final call in the application
        System.setOut(systemOut);
        System.setErr(systemErr);
        System.setIn(systemIn);
    }
}
