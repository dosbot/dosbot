package com.itwookie.logger;

import com.itwookie.utils.Listener;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.SimpleAttributeSet;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.HashMap;
import java.util.Map;

public class MultiTabLogger {
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    private JTextField textField1;
    private JFrame frame;
    private Map<String, LoggerTab> tabs = new HashMap<>();
    private Listener window_close = null;

    public void setCloseListener(Listener closeListener) {
        window_close = closeListener;
    }

    JFrame getFrame() {
        return frame;
    }

    public MultiTabLogger() {
        frame = new JFrame("Logger");
        frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        frame.setSize(new Dimension(800, 300));
        frame.setMinimumSize(frame.getSize());
        frame.add(panel1);
        frame.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                if (window_close != null)
                    window_close.fire();
            }

            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        });

        textField1.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    String line = textField1.getText();
                    textField1.setText("");
                    Logger.writeInput(line);
                }
            }
        });
        tabbedPane1.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int ai = tabbedPane1.getSelectedIndex();
                //find tab
                Component at = tabbedPane1.getComponentAt(ai);
                for (Map.Entry<String, LoggerTab> tab : tabs.entrySet()) {
                    if (tab.getValue().getPanel().equals(at)) {
                        tab.getValue().resetCounter();
                        tabbedPane1.setTitleAt(ai, tab.getKey());
                    }
                }
            }
        });
    }

    private LoggerTab getTab(String tag) {
        if (!tabs.containsKey(tag)) {
            LoggerTab tab = new LoggerTab();
            tabs.put(tag, tab);
            tabbedPane1.add(tag, tab.getPanel());
            return tab;
        } else
            return tabs.get(tag);
    }

    private void _log(String tag, String message, String p4c, SimpleAttributeSet tagStyle, SimpleAttributeSet textStyle) {
        LoggerTab tab = getTab(tag);
        tab.log(message, p4c, tagStyle, textStyle);
        int ti = tabbedPane1.indexOfComponent(tab.getPanel()), ai = tabbedPane1.getSelectedIndex();
        if (ai != ti) {
            int c = tab.getUnreadMessages();
            tabbedPane1.setTitleAt(ti, tag + " (" + c + ")");
        }
    }

    public void yell(String tag, String message) {
        _log(tag, message, "YELL", LoggerTab.highvisibility_tag, LoggerTab.highvisibility_text);
    }

    public void log(String tag, String message) {
        _log(tag, message, "INFO", LoggerTab.normal_tag, LoggerTab.normal_text);
    }

    public void warn(String tag, String message) {
        _log(tag, message, "WARN", LoggerTab.warning_tag, LoggerTab.warning_text);
    }

    public void err(String tag, String message) {
        _log(tag, message, "ERR ", LoggerTab.error_tag, LoggerTab.error_text);
    }

}
