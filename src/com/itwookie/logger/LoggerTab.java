package com.itwookie.logger;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;

public class LoggerTab {
    private JPanel panel1;
    private JTextPane textPane1;
    private JScrollPane scrollPane;
    private static boolean initialized = false;
    private int unreadMessages = 0;

    static SimpleAttributeSet highvisibility_text;
    static SimpleAttributeSet highvisibility_tag;
    static SimpleAttributeSet normal_text;
    static SimpleAttributeSet normal_tag;
    static SimpleAttributeSet error_text;
    static SimpleAttributeSet error_tag;
    static SimpleAttributeSet warning_text;
    static SimpleAttributeSet warning_tag;

    static {
        if (!initialized) {
            initialized = true;
            normal_text = new SimpleAttributeSet();
            StyleConstants.setForeground(normal_text, new Color(224, 224, 224));
            StyleConstants.setBackground(normal_text, Color.darkGray);
            StyleConstants.setBold(normal_text, false);
            StyleConstants.setFontFamily(normal_text, "Consolas");
            StyleConstants.setFontSize(normal_text, 12);

            normal_tag = new SimpleAttributeSet(normal_text);
            StyleConstants.setForeground(normal_tag, new Color(180, 180, 180));
            StyleConstants.setBold(normal_tag, true);

            highvisibility_text = new SimpleAttributeSet(normal_text);
            StyleConstants.setForeground(highvisibility_text, new Color(100, 255, 100));

            highvisibility_tag = new SimpleAttributeSet(normal_tag);
            StyleConstants.setForeground(highvisibility_tag, new Color(80, 200, 80));

            error_text = new SimpleAttributeSet(normal_text);
            StyleConstants.setForeground(error_text, new Color(255, 100, 100));

            error_tag = new SimpleAttributeSet(normal_tag);
            StyleConstants.setForeground(error_tag, new Color(200, 80, 80));

            warning_text = new SimpleAttributeSet(normal_text);
            StyleConstants.setForeground(warning_text, new Color(255, 185, 100));

            warning_tag = new SimpleAttributeSet(normal_tag);
            StyleConstants.setForeground(warning_tag, new Color(200, 160, 80));
        }
    }

    public LoggerTab() {
        textPane1.setBackground(Color.darkGray);
        textPane1.setForeground(Color.lightGray);
    }

    public JPanel getPanel() {
        return panel1;
    }

    public void log(String line, String p4c, SimpleAttributeSet tag_set, SimpleAttributeSet text_set) {
        line = line + '\n';
        StyledDocument doc = textPane1.getStyledDocument();
        try {
            if (doc.getLength() > 10000) {
                doc.remove(0, doc.getLength() - 10000);
            }
            doc.insertString(doc.getLength(), "[" + p4c + "] ", tag_set);
            doc.insertString(doc.getLength(), line, text_set);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
        try {
            JScrollBar bar = scrollPane.getVerticalScrollBar();
            bar.setValue(bar.getMaximum());
        } catch (Exception ignore) {
            //frame not yet fully visible
        }
    }

    public void resetCounter() {
        unreadMessages = 0;
    }

    public int getUnreadMessages() {
        return ++unreadMessages;
    }
}
