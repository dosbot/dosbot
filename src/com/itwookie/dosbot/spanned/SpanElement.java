package com.itwookie.dosbot.spanned;

import java.net.URI;

public abstract class SpanElement {

    public SpanElement(int firstIndex, int lastIndex) {
        if (lastIndex < firstIndex) throw new IllegalArgumentException("last index before first index");
        indexFirst = firstIndex;
        indexLast = lastIndex;
    }

    protected int indexFirst, indexLast; //both inclusive

    /**
     * @param size the emote/icon size, possible values may vary
     * @return the url for this emote
     */
    public abstract URI toURI(int size);

    /**
     * Example placeholders could be <code>{ffz:emoteID}</code> or <code>{bttv:emoteid}</code>
     *
     * @return a placeholder string formatted like <code>{emoteID}</code>
     */
    public abstract String toPlaceholder();

    /**
     * checks if another span overlaps this span as already-part of a SpannedString.
     * This function should not be order depending
     *
     * @param other a new emote
     * @return true if overlap was detected
     */
    public boolean overlaps(SpanElement other) {
        return ((indexFirst >= other.indexFirst && indexFirst <= other.indexLast) || //this emote contains the start point
                (indexLast >= other.indexFirst && indexLast <= other.indexLast) || //this emote contains the end point
                (indexFirst < other.indexFirst && indexLast > other.indexLast)); //the new emote tries to wrap over this emote
    }

    /**
     * This method creates a copy of this span with the new given boundaries
     *
     * @return a copied SpanElement of this elements class with new bounds
     */
    public abstract SpanElement clip(int newStart, int newEnd);

}