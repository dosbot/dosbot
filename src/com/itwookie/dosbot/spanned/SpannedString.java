package com.itwookie.dosbot.spanned;

import org.intellij.lang.annotations.MagicConstant;

import java.util.*;

public class SpannedString implements CharSequence {

    static class SpanSorter implements Comparator<SpanElement> {
        @Override
        public int compare(SpanElement o1, SpanElement o2) {
            return Integer.compare(o1.indexFirst, o2.indexFirst);
        }
    }

    protected char[] chars;
    protected Set<SpanElement> inserts = new TreeSet<>(new SpanSorter());

    protected SpannedString() {
    }

    public SpannedString(String message, String emoteSpanDescriptor) {
        chars = message.toCharArray();
        if (emoteSpanDescriptor.isEmpty()) return;
        String[] emotes = emoteSpanDescriptor.split("/");
        for (String emote : emotes) {
            if (emote.isEmpty()) continue;
            String[] parts = emote.split("[:,]");
            if (parts.length == 1) throw new IllegalArgumentException("Missing emote bounds");
            int eid = Integer.parseInt(parts[0]);
            for (int i = 1; i < parts.length; i++) {
                String[] se = parts[i].split("-");
                if (se.length != 2) throw new IllegalArgumentException("Emote bounds invalid");
                int s = Integer.parseInt(se[0]), e = Integer.parseInt(se[1]);
                if (s < 0 || e >= chars.length) throw new IllegalArgumentException("Emote bounds outside string");
                addSpan(new EmoteSpan(eid, s, e));
            }
        }
    }

    @Override
    public int length() {
        return chars.length;
    }

    @Override
    public char charAt(int index) {
        return chars[index];
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        SpannedString subString = new SpannedString();
        subString.chars = Arrays.copyOfRange(chars, start, end);
        for (SpanElement span : inserts) {
            //span completely outside
            if (span.indexLast < start || span.indexFirst >= end) continue;

            int s = span.indexFirst, e = span.indexLast;
            if (span.indexFirst < start) {
                // text HeyGuys words
                //       s----------e
                //span cut with start
                s = 0;
                e -= start;
            }
            if (span.indexLast >= end) {
                // text HeyGuys words
                // s----------e
                //span cut at end
                e = end - 1;
            }
            subString.addSpan(span.clip(s, e));
        }
        return null;
    }

    /**
     * @return true if the character at index is spanned by an emote
     */
    protected boolean hasSpanAt(int index) {
        if (index < 0 || index >= chars.length) return false;
        for (SpanElement span : inserts) {
            if (index >= span.indexFirst && index <= span.indexLast) return true;
        }
        return false;
    }

    /**
     * @return the emote at this index or null if there's not emote
     */
    protected SpanElement spanAt(int index) {
        if (index < 0 || index >= chars.length) return null;
        for (SpanElement span : inserts) {
            if (index >= span.indexFirst && index <= span.indexLast) return span;
        }
        return null;
    }

    @Override
    public String toString() {
        return new String(chars);
    }

    /**
     * spans are replaced by &lt;img&gt;-tags with the correct url
     *
     * @param size the emote size, values from 1-3 are allowed
     * @return the html version for this SpannedString as String
     */
    public String toHTML(@MagicConstant(intValues = {1, 2, 3}) int size) {
        if (size < 1 || size > 3) throw new IllegalArgumentException("Illegal size");
        StringBuilder sb = new StringBuilder(chars.length);
        int index = 0;
        for (SpanElement emote : inserts) {
            if (emote.indexFirst > index) {
                sb.append(this, index, emote.indexFirst);
                sb.append(String.format("<img src=\"%s\" />", emote.toURI(size)));
                index = emote.indexLast + 1;
            }
        }
        if (index <= chars.length) {
            sb.append(this, index, chars.length);
        }
        return sb.toString();
    }

    /**
     * creates a placeholder string where emote names are replaced with
     * placeholders formatted like <code>{emoteID}</code>
     *
     * @return placeholder String for this SpannedString
     */
    public String toPlaceholderString() {
        StringBuilder sb = new StringBuilder(chars.length);
        int index = 0;
        for (SpanElement emote : inserts) {
            if (emote.indexFirst > index) {
                sb.append(this, index, emote.indexFirst);
                sb.append(emote.toPlaceholder());
                index = emote.indexLast + 1;
            }
        }
        if (index <= chars.length) {
            sb.append(this, index, chars.length);
        }
        return sb.toString();
    }

    /**
     * remove emote text and shrink spans to length 0
     *
     * @return a SpannedString that does no longer contain the emote names
     */
    public SpannedString strip() {
        SpannedString res = new SpannedString();
        StringBuilder sb = new StringBuilder(chars.length);
        int index = 0;
        for (SpanElement emote : inserts) {
            if (emote.indexFirst > index) {
                sb.append(this, index, emote.indexFirst);
                res.addSpan(emote.clip(emote.indexFirst, emote.indexFirst));
                index = emote.indexLast + 1;
            }
        }
        if (index <= chars.length) {
            sb.append(this, index, chars.length);
        }
        res.chars = sb.toString().toCharArray();
        return res;
    }

    /**
     * create a emoteless SpannedString version of the string in case an argument requires such.<br>
     * Note that this does not auto replace emote names with EmoteSpans
     *
     * @return a SpannedString version of String
     */
    public static SpannedString fromString(String string) {
        SpannedString s = new SpannedString();
        s.chars = string.toCharArray();
        return s;
    }

    /**
     * Add a emote span to this SpannedString. Note that Emote Spans can't overlap!
     *
     * @param span the emote to add
     */
    public void addSpan(SpanElement span) {
        for (SpanElement emote : inserts) {
            if (emote.overlaps(span)) //the new emote tries to wrap over this emote
                throw new IllegalArgumentException("Emote overlaps");
        }
        inserts.add(span);
    }

    public Collection<SpanElement> getEmtes() {
        return inserts;
    }
}
