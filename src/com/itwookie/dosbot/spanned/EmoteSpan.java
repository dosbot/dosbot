package com.itwookie.dosbot.spanned;

import org.intellij.lang.annotations.MagicConstant;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * HOW TO overwrite this class for custom emotes:<br>
 * - extend this class as usual, <br>- provide default overrides for Constructors, <br>- overwrite toURI method with
 * you custom image url<br>- overwrite toPlaceholder to use a prefix<br>
 * the last step is only a recommendation, but you should somehow change it to be able to distinguish between default
 * emotes.<br>
 * Example placeholders could be <code>{ffz:emoteID}</code> or <code>{bttv:emoteid}</code>
 */
public class EmoteSpan extends SpanElement {

    public EmoteSpan(int emote, int firstIndex, int lastIndex) {
        super(firstIndex, lastIndex);
        emoteID = emote;
    }

    protected int emoteID;

    /**
     * @param size the emote size, values from 1-3 are allowed
     * @return the url for this emote
     */
    public URI toURI(@MagicConstant(intValues = {1, 2, 3}) int size) {
        if (size < 1 || size > 3) throw new IllegalArgumentException("Illegal size");
        try {
            return new URI(String.format("http://static-cdn.jtvnw.net/emoticons/v1/%d/%d.0", emoteID, size));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @return a placeholder string formatted like <code>{emoteID}</code>
     */
    public String toPlaceholder() {
        return String.format("{%d}", emoteID);
    }

    @Override
    public SpanElement clip(int newStart, int newEnd) {
        return new EmoteSpan(emoteID, newStart, newEnd);
    }
}