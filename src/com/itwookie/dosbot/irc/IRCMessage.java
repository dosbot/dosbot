package com.itwookie.dosbot.irc;

import com.itwookie.dosbot.irc.command.*;

public class IRCMessage {
    private TagBag tags = new TagBag();
    private String prefix;
    private String command;
    private String params; //contains everything after irc command
    private AbstractCommand parsedCommand;

    private final String nospcrlfcl = "[^ \\r\\n:]";

    public IRCMessage(String raw) {
        if (raw.charAt(0) == '@') { //tags string
            String tmp = raw.substring(0, raw.indexOf(' '));
            raw = raw.substring(tmp.length() + 1);
            tags.fromString(tmp);
        }
        if (raw.charAt(0) == ':') {
            prefix = raw.substring(0, raw.indexOf(' '));
            raw = raw.substring(prefix.length() + 1);
        } else {
            prefix = "";
        }
        if (raw.indexOf(' ') > 0) {
            command = raw.substring(0, raw.indexOf(' '));
            params = raw.substring(command.length() + 1);
        } else {
            command = raw;
            params = "";
        }
        parsedCommand = buildCommand();
    }

    public String getRaw() {
        return (!prefix.isEmpty() ? prefix + ' ' : "") + command + (!params.isEmpty() ? ' ' + params : "");
    }

    @Override
    public String toString() {
        return (!prefix.isEmpty() ? prefix + ' ' : "") + demystifyMagicNumber(command) + (!params.isEmpty() ? ' ' + params : "");
    }

    /**
     * @return the tags information prefixing the message
     */
    public TagBag getTags() {
        return tags;
    }

    /**
     * @return irc message part "prefix" or empty string if missing
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * @return the username from prefix if possible, or null otherwise
     */
    public String getUser() {
        int iEX = prefix.indexOf('!');
        int iAT = prefix.indexOf('@');
        if (iEX >= 0 && iAT > iEX) {
            return prefix.substring(1, iEX);
        }
        return null;
    }

    /**
     * @return irc command part "prefix" or empty string if missing
     */
    public String getCommand() {
        return command;
    }

    /**
     * @return irc params part "prefix" or empty string if missing
     */
    public String getParams() {
        return params;
    }

    public AbstractCommand toCommand() {
        return parsedCommand;
    }

    private AbstractCommand buildCommand() {
        if (command.matches("^[0-9]{3}$"))
            return new icReply(this);
        switch (command.toUpperCase()) {
            case "CAP":
                return new icCap(this);
            case "JOIN":
                return new icJoin(this);
            case "MODE":
                return new icMode(this);
            case "PART":
                return new icPart(this);
            case "PING":
                return new icPing(this);
            case "PONG":
                return new icPong(this);
            case "PRIVMSG":
                return new icPrivmsg(this);
            case "ROOMSTATE":
                return new icTwitchRoomstate(this);
            case "USERNOTICE":
                return new icTwitchUsernotice(this);
            case "USERSTATE":
                return new icTwitchUserstate(this);
            case "GLOBALUSERSTATE":
                return new icTwitchGlobalUserstate(this);
            default:
                return new icDummyUnknown(this);
        }
    }

    public static String demystifyMagicNumber(String command) {
        int v = -1;
        if (command.length() != 3) return command;
        try {
            v = Integer.valueOf(command);
        } catch (NumberFormatException ignore) {
            return command;
        }
        switch (v) {
            case 1:
                return "RPL_WELCOME";
            case 2:
                return "RPL_YOURHOST";
            case 3:
                return "RPL_CREATED";
            case 4:
                return "RPL_MYINFO";
            case 5:
                return "RPL_BOUNCE";
            case 301:
                return "RPL_AWAY";
            case 302:
                return "RPL_USERHOST";
            case 303:
                return "RPL_ISON";
            case 305:
                return "RPL_UNAWAY";
            case 306:
                return "RPL_NOWAWAY";
            case 311:
                return "RPL_WHOISUSER";
            case 312:
                return "RPL_WHOISSERVER";
            case 313:
                return "RPL_WHOISOPERATOR";
            case 317:
                return "RPL_WHOISIDLE";
            case 318:
                return "RPL_ENDOFWHOIS";
            case 319:
                return "RPL_WHOISCHANNELS";
            case 314:
                return "RPL_WHOWASUSER";
            case 369:
                return "RPL_ENDOFWHOWAS";
            case 321:
                return "RPL_LISTSTART";
            case 322:
                return "RPL_LIST";
            case 323:
                return "RPL_LISTEND";
            case 325:
                return "RPL_UNIQOPIS";
            case 324:
                return "RPL_CHANNELMODEIS";
            case 331:
                return "RPL_NOTOPIC";
            case 332:
                return "RPL_TOPIC";
            case 341:
                return "RPL_INVITING";
            case 342:
                return "RPL_SUMMONING";
            case 346:
                return "RPL_INVITELIST";
            case 347:
                return "RPL_ENDOFINVITELIST";
            case 348:
                return "RPL_EXCEPTLIST";
            case 349:
                return "RPL_ENDOFEXCEPTLIST";
            case 351:
                return "RPL_VERSION";
            case 352:
                return "RPL_WHOREPLY";
            case 315:
                return "RPL_ENDOFWHO";
            case 353:
                return "RPL_NAMREPLY";
            case 366:
                return "RPL_ENDOFNAMES";
            case 364:
                return "RPL_LINKS";
            case 365:
                return "RPL_ENDOFLINKS";
            case 367:
                return "RPL_BANLIST";
            case 368:
                return "RPL_ENDOFBANLIST";
            case 371:
                return "RPL_INFO";
            case 374:
                return "RPL_ENDOFINFO";
            case 375:
                return "RPL_MOTDSTART";
            case 372:
                return "RPL_MOTD";
            case 376:
                return "RPL_ENDOFMOTD";
            case 381:
                return "RPL_YOUREOPER";
            case 382:
                return "RPL_REHASHING";
            case 383:
                return "RPL_YOURESERVICE";
            case 391:
                return "RPL_TIME";
            case 392:
                return "RPL_USERSSTART";
            case 393:
                return "RPL_USERS";
            case 394:
                return "RPL_ENDOFUSERS";
            case 395:
                return "RPL_NOUSERS";
            case 200:
                return "RPL_TRACELINK";
            case 201:
                return "RPL_TRACECONNECTING";
            case 202:
                return "RPL_TRACEHANDSHAKE";
            case 203:
                return "RPL_TRACEUNKNOWN";
            case 204:
                return "RPL_TRACEOPERATOR";
            case 205:
                return "RPL_TRACEUSER";
            case 206:
                return "RPL_TRACESERVER";
            case 207:
                return "RPL_TRACESERVICE";
            case 208:
                return "RPL_TRACENEWTYPE";
            case 209:
                return "RPL_TRACECLASS";
            case 210:
                return "RPL_TRACERECONNECT";
            case 261:
                return "RPL_TRACELOG";
            case 262:
                return "RPL_TRACEEND";
            case 211:
                return "RPL_STATSLINKINFO";
            case 212:
                return "RPL_STATSCOMMANDS";
            case 219:
                return "RPL_ENDOFSTATS";
            case 242:
                return "RPL_STATSUPTIME";
            case 243:
                return "RPL_STATSOLINE";
            case 221:
                return "RPL_UMODEIS";
            case 234:
                return "RPL_SERVLIST";
            case 235:
                return "RPL_SERVLISTEND";
            case 251:
                return "RPL_LUSERCLIENT";
            case 252:
                return "RPL_LUSEROP";
            case 253:
                return "RPL_LUSERUNKNOWN";
            case 254:
                return "RPL_LUSERCHANNELS";
            case 255:
                return "RPL_LUSERME";
            case 256:
                return "RPL_ADMINME";
            case 257:
                return "RPL_ADMINLOC1";
            case 258:
                return "RPL_ADMINLOC2";
            case 259:
                return "RPL_ADMINEMAIL";
            case 263:
                return "RPL_TRYAGAIN";
            case 401:
                return "ERR_NOSUCHNICK";
            case 402:
                return "ERR_NOSUCHSERVER";
            case 403:
                return "ERR_NOSUCHCHANNEL";
            case 404:
                return "ERR_CANNOTSENDTOCHAN";
            case 405:
                return "ERR_TOOMANYCHANNELS";
            case 406:
                return "ERR_WASNOSUCHNICK";
            case 407:
                return "ERR_TOOMANYTARGETS";
            case 408:
                return "ERR_NOSUCHSERVICE";
            case 409:
                return "ERR_NOORIGIN";
            case 411:
                return "ERR_NORECIPIENT";
            case 412:
                return "ERR_NOTEXTTOSEND";
            case 413:
                return "ERR_NOTOPLEVEL";
            case 414:
                return "ERR_WILDTOPLEVEL";
            case 415:
                return "ERR_BADMASK";
            case 421:
                return "ERR_UNKNOWNCOMMAND";
            case 422:
                return "ERR_NOMOTD";
            case 423:
                return "ERR_NOADMININFO";
            case 424:
                return "ERR_FILEERROR";
            case 431:
                return "ERR_NONICKNAMEGIVEN";
            case 432:
                return "ERR_ERRONEUSNICKNAME";
            case 433:
                return "ERR_NICKNAMEINUSE";
            case 436:
                return "ERR_NICKCOLLISION";
            case 437:
                return "ERR_UNAVAILRESOURCE";
            case 441:
                return "ERR_USERNOTINCHANNEL";
            case 442:
                return "ERR_NOTONCHANNEL";
            case 443:
                return "ERR_USERONCHANNEL";
            case 444:
                return "ERR_NOLOGIN";
            case 445:
                return "ERR_SUMMONDISABLED";
            case 446:
                return "ERR_USERSDISABLED";
            case 451:
                return "ERR_NOTREGISTERED";
            case 461:
                return "ERR_NEEDMOREPARAMS";
            case 462:
                return "ERR_ALREADYREGISTRED";
            case 463:
                return "ERR_NOPERMFORHOST";
            case 464:
                return "ERR_PASSWDMISMATCH";
            case 465:
                return "ERR_YOUREBANNEDCREEP";
            case 466:
                return "ERR_YOUWILLBEBANNED";
            case 467:
                return "ERR_KEYSET";
            case 471:
                return "ERR_CHANNELISFULL";
            case 472:
                return "ERR_UNKNOWNMODE";
            case 473:
                return "ERR_INVITEONLYCHAN";
            case 474:
                return "ERR_BANNEDFROMCHAN";
            case 475:
                return "ERR_BADCHANNELKEY";
            case 476:
                return "ERR_BADCHANMASK";
            case 477:
                return "ERR_NOCHANMODES";
            case 478:
                return "ERR_BANLISTFULL";
            case 481:
                return "ERR_NOPRIVILEGES";
            case 482:
                return "ERR_CHANOPRIVSNEEDED";
            case 483:
                return "ERR_CANTKILLSERVER";
            case 484:
                return "ERR_RESTRICTED";
            case 485:
                return "ERR_UNIQOPPRIVSNEEDED";
            case 491:
                return "ERR_NOOPERHOST";
            case 501:
                return "ERR_UMODEUNKNOWNFLAG";
            case 502:
                return "ERR_USERSDONTMATCH";

            default:
                return command;
        }
    }

}
