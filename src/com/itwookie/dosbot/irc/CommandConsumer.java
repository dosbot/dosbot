package com.itwookie.dosbot.irc;

import com.itwookie.dosbot.irc.command.AbstractCommand;

import java.util.function.Consumer;

/**
 * Consuming a raw string as command
 */
@FunctionalInterface
public interface CommandConsumer extends Consumer<IRCMessage> {
    default void accept(IRCMessage t) {
        accept(t.toCommand());
    }

    void accept(AbstractCommand t);
}
