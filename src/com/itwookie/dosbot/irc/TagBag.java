package com.itwookie.dosbot.irc;

import java.util.HashMap;

public class TagBag extends HashMap<String, String> {
    /**
     * @param s the tags string including @ prefix
     */
    public void fromString(String s) {
        String[] p = s.substring(1).split(";");
        for (String e : p) {
            String k = e.substring(0, e.indexOf('='));
            String v = unescapeValue(e.substring(k.length() + 1));
            put(k, v);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Entry<String, String> e : entrySet()) {
            sb.append("; ");
            sb.append(e.getKey());
            sb.append('=');
            sb.append(e.getValue());
        }
        return sb.length() < 2 ? "<EMPTY>" : sb.substring(2);
    }

    /**
     * You should not need this, values in TagBag instances are usually already unescaped<br>
     * Unscape a value according to specs https://ircv3.net/specs/core/message-tags-3.2.html
     */
    public static String unescapeValue(String escaped) {
        StringBuilder unescaped = new StringBuilder(escaped.length());
        boolean escapeOpen = false;
        for (char c : escaped.toCharArray()) {
            if (escapeOpen) {
                switch (c) {
                    case ':': {
                        unescaped.append(';');
                        break;
                    }
                    case 's': {
                        unescaped.append(' ');
                        break;
                    }
                    case '\\': {
                        unescaped.append('\\');
                        break;
                    }
                    case 'r': {
                        unescaped.append('\r');
                        break;
                    }
                    case 'n': {
                        unescaped.append('\n');
                        break;
                    }
                    default: {
                        throw new IllegalArgumentException("Illegal escape string '\\" + c + "'");
                    }
                }
                escapeOpen = false;
            } else {
                if (c == '\\') escapeOpen = true;
                else unescaped.append(c);
            }
        }
        if (escapeOpen) throw new IllegalArgumentException("Unfinished escape string at end of input");
        return unescaped.toString();
    }

    /**
     * Should not be required for anything, this is just here for completions sake<br>
     * Escape a value according to specs https://ircv3.net/specs/core/message-tags-3.2.html
     */
    public static String escapeValue(String escaped) {
        StringBuilder unescaped = new StringBuilder(escaped.length());
        for (char c : escaped.toCharArray()) {
            switch (c) {
                case ';': {
                    unescaped.append("\\:");
                    break;
                }
                case ' ': {
                    unescaped.append("\\s");
                    break;
                }
                case '\\': {
                    unescaped.append("\\\\");
                    break;
                }
                case '\r': {
                    unescaped.append("\\r");
                    break;
                }
                case '\n': {
                    unescaped.append("\\n");
                    break;
                }
                default: {
                    unescaped.append(c);
                }
            }
        }
        return unescaped.toString();
    }
}
