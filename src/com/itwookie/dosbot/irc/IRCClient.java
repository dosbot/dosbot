package com.itwookie.dosbot.irc;

import com.itwookie.dosbot.TwitchClient;
import com.itwookie.dosbot.events.chat.RoomChangeEvent;
import com.itwookie.dosbot.irc.command.AbstractCommand;
import com.itwookie.dosbot.telnet.QueueHandler;
import com.itwookie.dosbot.telnet.TelnetClient;
import com.itwookie.dosbot.twitch.Badges;
import com.itwookie.dosbot.twitch.Channel;
import com.itwookie.dosbot.twitch.User;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;

public class IRCClient {

    private Channel channel = null;
    private QueueHandler<IRCMessage> inMessageQueue;
    private TelnetClient telnet;

    private IRCMessageConsumer messageHandler = (msg) -> {
        if (!msg.getPrefix().isEmpty() && msg.getPrefix().charAt(0) == '#') {
            if (channel == null || !channel.getName().equalsIgnoreCase(msg.getPrefix().substring(1)))
                channel = new Channel(msg.getPrefix().substring(1));
        }
        if (channel != null) {
            switch (msg.getCommand().toUpperCase()) {
                case "JOIN": {
                    if (!channel.getUser(msg.getUser()).isPresent())
                        channel.__innternalAddUser(new User(msg.getUser(), msg.getTags()), true);
                    else
                        channel.getUser(msg.getUser()).ifPresent(user -> user.update(msg.getTags()));
                    break;
                }
                case "PART": {
                    channel.getUser(msg.getUser()).ifPresent(u -> channel.__internalRemoveUser(u));
                    break;
                }
                case "ROOMSTATE": {
                    RoomChangeEvent event = RoomChangeEvent.fromTags(msg.getTags());
                    TwitchClient.getEventBus().fire(event);
                    channel.update(msg.getTags()); //not using event since "hackable"/mutable

                    //channel-as-user update
                    TagBag tags = new TagBag();
                    tags.put("user-id", String.valueOf(channel.getRoomID()));
                    tags.put("mod", "1");
                    tags.put("badges", Badges.BROADCASTER + "/1");
                    channel.asUser().update(tags);
                    break;
                }
                case "PRIVMSG": {
                    channel.getUser(msg.getUser())
                            .orElseGet(() -> {
                                User u = new User(msg.getUser(), msg.getTags());
                                channel.__innternalAddUser(u, true);
                                return u;
                            })
                            .update(msg.getTags());
                    break;
                }
                case "USERSTATE": {
                    //since this message does not contain a login name
                    //i assume it can only be received for self
                    TwitchClient.getSelf().update(msg.getTags());
                    break;
                }
                case "GLOBALUSERSTATE": {
                    TwitchClient.getSelf().update(msg.getTags());
                    break;
                }
                case "USERNOTICE": {
                    channel.getUser(msg.getUser())
                            .ifPresent(u -> u.notify(msg.getTags()));
                    break;
                }
                case "353":
                case "RPL_NAMREPLY": {
                    String list = msg.getParams();
                    list = list.substring(list.indexOf(" :") + 2);
                    String[] names = list.split("\\s");
                    for (String name : names)
                        getChannel().__innternalAddUser(new User(name, new TagBag()), false);
                    break;
                }
            }
        }
    };

    public IRCClient(InetSocketAddress address) {
        telnet = new TelnetClient(address);
        inMessageQueue = new QueueHandler<>(true);
        telnet.addMessageListener(telnet -> inMessageQueue.enqueueMessage(new IRCMessage(telnet)));
        inMessageQueue.addMessageHandler(messageHandler);
        inMessageQueue.start();
    }

    public IRCClient(InetAddress address, int port) throws IOException {
        this(new InetSocketAddress(address, port));
    }

    public void useChannel(String channelName) {
        if (channel == null)
            channel = new Channel(channelName.toLowerCase());
    }

    public Channel getChannel() {
        return channel;
    }

    /**
     * @return self for chaining
     */
    public IRCClient send(AbstractCommand message) throws IllegalStateException {
        telnet.send(message.stringValue());
        return this;
    }

    /**
     * waits until all queues are empty.
     *
     * @return self for chaining
     */
    public IRCClient await() {
        while (!telnet.emptyQueues()) Thread.yield();
        return this;
    }

    /**
     * waits until all queues are empty, but at least the specified time.
     *
     * @return self for chaining
     */
    public IRCClient await(int ms) {
        long targetTime = System.currentTimeMillis() + ms;
        while (!telnet.emptyQueues() || System.currentTimeMillis() < targetTime) {
            try {
                Thread.sleep(Math.max(0, targetTime - System.currentTimeMillis()));
            } catch (InterruptedException ignore) {/**/}
        }
        return this;
    }

    /**
     * wait once for a packet of a specific package, then unregister the listener.
     * blocks the calling thread until the package was received, handler should stay unaffected.<br>
     * Don't put all your login calls like this, might be tempting but is not stack optimized!
     *
     * @param command   the command class to filter on
     * @param predicate optional predicate to check on the specified commands, cast should be safe
     * @param handler   a optional one-time handler to act on the command
     */
    public <T extends AbstractCommand> IRCClient await(Class<T> command, Predicate<AbstractCommand> predicate, CommandConsumer handler) {
        CompletableFuture<AbstractCommand> future = new CompletableFuture<>();
        CommandConsumer c = t -> {
            if (command.isAssignableFrom(t.getClass()) && (predicate == null || predicate.test(t))) {
                future.complete(t);
            }
        };
        inMessageQueue.addMessageHandler(c);
        try {
            if (handler != null)
                handler.accept(future.get());
            else
                future.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        inMessageQueue.removeMessageHander(c);
        return this;
    }

    /**
     * convenience method for converting using the IRCMessage constructor.<br>
     * Usable as .map(IRCClient::asIRCMessage)
     */
    public static IRCMessage asIRCMessage(String TelnetMessage) {
        return new IRCMessage(TelnetMessage);
    }

    /**
     * convenience method for new IRCMessage(telnetMessage).toCommand()<br>
     * Usable as .map(IRCClient::asCommand)
     */
    public static AbstractCommand asCommand(String TelnetMessage) {
        return new IRCMessage(TelnetMessage).toCommand();
    }

    /**
     * @see TelnetClient
     */
    public void addMessageListener(CommandConsumer commandConsumer) {
        inMessageQueue.addMessageHandler(commandConsumer);
    }

    /**
     * @see TelnetClient
     */
    public void addMessageFilter(CommandPredicate commandPredicate) {
        inMessageQueue.addMessageFilter(commandPredicate);
    }

    /**
     * @return the telnet client used for this IRC connection
     */
    public TelnetClient getTelnet() {
        return telnet;
    }

    /**
     * closes the telnet connection for this irc client
     */
    public void close() {
        telnet.close();
        inMessageQueue.close();
    }
}
