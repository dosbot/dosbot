package com.itwookie.dosbot.irc;

public class IRCMessageException extends RuntimeException {
    private IRCMessage response;

    public IRCMessageException(IRCMessage msg) {
        super();
        response = msg;
    }

    public IRCMessageException(String message, IRCMessage msg) {
        super(message);
        response = msg;
    }

    public IRCMessageException(String message, Throwable cause, IRCMessage msg) {
        super(message, cause);
        response = msg;
    }

    public IRCMessageException(Throwable cause, IRCMessage msg) {
        super(cause);
        response = msg;
    }

    public IRCMessageException(String message, Throwable cause, IRCMessage msg, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        response = msg;
    }

    public IRCMessage getResponse() {
        return response;
    }
}
