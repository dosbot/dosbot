package com.itwookie.dosbot.irc.command;

import com.itwookie.dosbot.irc.IRCMessage;

import java.util.Optional;

public class icReply implements AbstractCommand {
    IRCMessage internal;
    String reply, message;

    public icReply(IRCMessage msg) {
        internal = msg;
        reply = IRCMessage.demystifyMagicNumber(msg.getCommand());
        message = msg.getParams();
        netraw = msg;
    }

    private IRCMessage netraw = null;

    @Override
    public Optional<IRCMessage> getMessageRaw() {
        return Optional.ofNullable(netraw);
    }

    @Override
    public String toString() {
        return internal.toString();
    }

    @Override
    public String stringValue() {
        return internal.getRaw();
    }

    public String getReply() {
        return reply;
    }

    public String getMessage() {
        return message;
    }
}
