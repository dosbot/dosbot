package com.itwookie.dosbot.irc.command;

import com.itwookie.dosbot.irc.IRCMessage;

import java.util.Optional;

/**
 * enable additional capabilities
 */
public class icCap implements AbstractCommand {

    private String capability;
    private boolean ack = false;

    public icCap(String capability) {
        this.capability = capability;
    }

    public icCap(IRCMessage msg) {
        ack = msg.getParams().toUpperCase().contains("* ACK");
        capability = msg.getParams().split(" :")[1];
        netraw = msg;
    }

    private IRCMessage netraw = null;

    @Override
    public Optional<IRCMessage> getMessageRaw() {
        return Optional.ofNullable(netraw);
    }

    @Override
    public String toString() {
        return "Requesting capability " + capability;
    }

    @Override
    public String stringValue() {
        return "CAP REQ :" + capability;
    }

    public String getCapability() {
        return capability;
    }

    public boolean isAcknowledged() {
        return ack;
    }
}
