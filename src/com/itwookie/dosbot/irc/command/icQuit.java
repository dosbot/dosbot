package com.itwookie.dosbot.irc.command;

import com.itwookie.dosbot.irc.IRCMessage;

import java.util.Optional;

public class icQuit implements AbstractCommand {

    private String message;

    public icQuit(String message) {
        this.message = message;
    }

    @Override
    public Optional<IRCMessage> getMessageRaw() {
        return Optional.empty();
    }

    @Override
    public String toString() {
        return "Quit: " + message;
    }

    @Override
    public String stringValue() {
        return "QUIT :" + message;
    }

    public String getMessage() {
        return message;
    }
}
