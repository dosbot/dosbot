package com.itwookie.dosbot.irc.command;

import com.itwookie.dosbot.irc.IRCMessage;

import java.util.Optional;
import java.util.regex.Pattern;

public class icNick implements AbstractCommand {

    private static final Pattern nickname = Pattern.compile("^[A-Za-z{|}^_`\\[\\]\\\\][A-Za-z0-9{|}^_`\\[\\]\\\\-]*$");

    private String name;

    /**
     * @param userName the nickname as defined in RFC 2812 section 2.3.1
     */
    public icNick(String userName) {
        if (!nickname.matcher(userName).matches())
            throw new IllegalArgumentException("Nickname is not a string as defined by rfc");
        name = userName;
    }

    @Override
    public Optional<IRCMessage> getMessageRaw() {
        return Optional.empty();
    }

    @Override
    public String toString() {
        return "Nickname " + name;
    }

    @Override
    public String stringValue() {
        return "NICK " + name;
    }

    public String getNickname() {
        return name;
    }
}
