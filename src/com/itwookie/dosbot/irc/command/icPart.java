package com.itwookie.dosbot.irc.command;

import com.itwookie.dosbot.irc.IRCMessage;

import java.util.Optional;
import java.util.regex.Pattern;

public class icPart implements AbstractCommand {

    private static final Pattern chanstring = Pattern.compile("^[^\\x00\\a\\r\\n ,:]+$");

    private String channel;
    private String user;

    /**
     * @param channelName the chanstring as defined in RFC 2812 section 2.3.1
     */
    public icPart(String channelName) {
        if (!chanstring.matcher(channelName).matches())
            throw new IllegalArgumentException("Channel name is not a chanstring by definition");
        channel = channelName;
    }

    public icPart(IRCMessage msg) {
        user = msg.getUser();
        channel = msg.getParams().substring(1);
        netraw = msg;
    }

    private IRCMessage netraw = null;

    @Override
    public Optional<IRCMessage> getMessageRaw() {
        return Optional.ofNullable(netraw);
    }

    @Override
    public String toString() {
        if (user == null)
            return "Part channel #" + channel;
        else
            return user + " parted #" + channel;
    }

    @Override
    public String stringValue() {
        return "PART #" + channel;
    }

    public String getChannel() {
        return channel;
    }

    public String getUser() {
        return user;
    }
}
