package com.itwookie.dosbot.irc.command;

import com.itwookie.dosbot.irc.IRCMessage;
import com.itwookie.dosbot.irc.TagBag;

import java.util.Optional;

/**
 * Anounce events to channel, can't be sent
 */
public class icTwitchGlobalUserstate implements AbstractCommand {

    private TagBag tags;

    public icTwitchGlobalUserstate(IRCMessage msg) {
        tags = msg.getTags();
        netraw = msg;
    }

    private IRCMessage netraw = null;

    @Override
    public Optional<IRCMessage> getMessageRaw() {
        return Optional.ofNullable(netraw);
    }

    @Override
    public String toString() {
        return "GlobalUsernotice: " + tags.getOrDefault("system-msg", tags.toString());
    }

    @Override
    public String stringValue() {
        return "";
    }

    public TagBag getTags() {
        return tags;
    }
}
