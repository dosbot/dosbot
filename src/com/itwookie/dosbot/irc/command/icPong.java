package com.itwookie.dosbot.irc.command;

import com.itwookie.dosbot.irc.IRCMessage;

import java.util.Optional;

public class icPong implements AbstractCommand {

    private String server;

    public icPong(String server) {
        this.server = server;
    }

    public icPong(IRCMessage msg) {
        server = msg.getParams().split(" ")[0];
        netraw = msg;
    }

    private IRCMessage netraw = null;

    @Override
    public Optional<IRCMessage> getMessageRaw() {
        return Optional.ofNullable(netraw);
    }

    @Override
    public String toString() {
        return "Pong to " + server;
    }

    @Override
    public String stringValue() {
        return "PING " + server;
    }

    public String getServer() {
        return server;
    }
}
