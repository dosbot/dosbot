package com.itwookie.dosbot.irc.command;

import com.itwookie.dosbot.irc.IRCMessage;

import java.util.Optional;

/**
 * instance should not be kept
 */
public class icPass implements AbstractCommand {

    private String tmp;

    public icPass(String passwd) {
        tmp = passwd;
    }

    @Override
    public Optional<IRCMessage> getMessageRaw() {
        return Optional.empty();
    }

    @Override
    public String toString() {
        return "<Password>";
    }

    @Override
    public String stringValue() {
        return "PASS " + tmp;
    }
}
