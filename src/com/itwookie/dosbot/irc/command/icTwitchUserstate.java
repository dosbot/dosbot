package com.itwookie.dosbot.irc.command;

import com.itwookie.dosbot.irc.IRCMessage;
import com.itwookie.dosbot.irc.TagBag;

import java.util.Optional;

/**
 * Anounce events to channel, can't be sent
 */
public class icTwitchUserstate implements AbstractCommand {

    private TagBag tags;
    private String channel;

    public icTwitchUserstate(IRCMessage msg) {
        tags = msg.getTags();
        channel = msg.getParams().split(" ")[0];
        netraw = msg;
    }

    private IRCMessage netraw = null;

    @Override
    public Optional<IRCMessage> getMessageRaw() {
        return Optional.ofNullable(netraw);
    }

    @Override
    public String toString() {
        return "Userstate: " + tags.getOrDefault("system-msg", tags.toString());
    }

    @Override
    public String stringValue() {
        return "";
    }

    public String getChannel() {
        return channel;
    }

    public TagBag getTags() {
        return tags;
    }
}
