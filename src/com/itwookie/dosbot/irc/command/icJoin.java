package com.itwookie.dosbot.irc.command;

import com.itwookie.dosbot.irc.IRCMessage;

import java.util.Optional;
import java.util.regex.Pattern;

public class icJoin implements AbstractCommand {

    private static final Pattern chanstring = Pattern.compile("^[^\\x00\\a\\r\\n ,:]+$");

    private String user;
    private String channel;

    /**
     * @param channelName the chanstring as defined in RFC 2812 section 2.3.1
     */
    public icJoin(String channelName) {
        if (!chanstring.matcher(channelName).matches())
            throw new IllegalArgumentException("Channel name is not a chanstring by definition");
        channel = channelName;
    }

    public icJoin(IRCMessage msg) {
        user = msg.getUser();
        channel = msg.getParams().substring(1);
        netraw = msg;
    }

    private IRCMessage netraw = null;

    @Override
    public Optional<IRCMessage> getMessageRaw() {
        return Optional.ofNullable(netraw);
    }

    @Override
    public String toString() {
        if (user == null)
            return "Join channel #" + channel;
        else
            return user + " joined #" + channel;
    }

    @Override
    public String stringValue() {
        return "JOIN #" + channel;
    }

    public String getChannel() {
        return channel;
    }

    public String getUser() {
        return user;
    }
}
