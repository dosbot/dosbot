package com.itwookie.dosbot.irc.command;

import com.itwookie.dosbot.irc.IRCMessage;
import com.itwookie.dosbot.irc.TagBag;

import java.util.Optional;
import java.util.regex.Pattern;

public class icPrivmsg implements AbstractCommand {

    private final static Pattern p_target_channel = Pattern.compile("^#[^\\x00\\a\\r\\n ,:]+$");
    private final static Pattern p_message = Pattern.compile("^[^\\r\\n]+$");

    private String target, message;
    private String user;
    private TagBag tags;

    /**
     * @param target the IRC-target. prefix with '#' to target channels
     */
    public icPrivmsg(String target, String message) {
        if (!p_target_channel.matcher(target).matches())
            throw new IllegalArgumentException("Currently PRIVMSG only supports channel targets");
        if (!p_message.matcher(target).matches())
            throw new IllegalArgumentException("Message can't contain a linebreak");
        this.target = target;
        this.message = message.trim().replaceAll("[\\s]+", " ");
    }

    public icPrivmsg(IRCMessage msg) {
        user = msg.getUser();
        tags = msg.getTags();
        String params = msg.getParams();
        target = params.substring(0, params.indexOf(" :"));
        message = params.substring(params.indexOf(" :") + 2);
        netraw = msg;
    }

    private IRCMessage netraw = null;

    @Override
    public Optional<IRCMessage> getMessageRaw() {
        return Optional.ofNullable(netraw);
    }

    @Override
    public String toString() {
        if (user == null)
            return target + ">: " + message;
        else
            return target + " " + tags.getOrDefault("display-name", user) + ": " + message;
    }

    @Override
    public String stringValue() {
        return "PRIVMSG " + target + " :" + message;
    }

    public String getTarget() {
        return target;
    }

    public String getMessage() {
        return message;
    }

    public String getUser() {
        return user;
    }

    public TagBag getTags() {
        return tags;
    }
}
