package com.itwookie.dosbot.irc.command;

import com.itwookie.dosbot.irc.IRCMessage;
import com.itwookie.dosbot.irc.TagBag;

import java.util.Optional;

/**
 * Receive roomstate updates
 */
public class icTwitchRoomstate implements AbstractCommand {

    private TagBag tags;
    private String channel;

    public icTwitchRoomstate(IRCMessage msg) {
        tags = msg.getTags();
        channel = msg.getParams().split(" ")[0].substring(1); //remove #
        netraw = msg;
    }

    private IRCMessage netraw = null;

    @Override
    public Optional<IRCMessage> getMessageRaw() {
        return Optional.ofNullable(netraw);
    }

    @Override
    public String toString() {
        return "Roomstate: " + tags.toString();
    }

    @Override
    public String stringValue() {
        return "";
    }

    /**
     * to validate target. use TwitchClient.getChannel() for actual channel
     */
    public String getChannel() {
        return channel;
    }
}
