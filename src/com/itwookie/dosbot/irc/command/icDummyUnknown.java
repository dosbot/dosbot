package com.itwookie.dosbot.irc.command;

import com.itwookie.dosbot.irc.IRCMessage;
import com.itwookie.dosbot.irc.TagBag;

import java.util.Optional;
import java.util.regex.Pattern;

public class icDummyUnknown implements AbstractCommand {

    private static final Pattern chanstring = Pattern.compile("^[^\\x00\\a\\r\\n ,:]+$");

    private String cap;
    private String user;
    private TagBag tags;

    public icDummyUnknown(IRCMessage msg) {
        tags = msg.getTags();
        user = msg.getUser();
        cap = msg.getRaw();
        netraw = msg;
    }

    private IRCMessage netraw = null;

    @Override
    public Optional<IRCMessage> getMessageRaw() {
        return Optional.ofNullable(netraw);
    }

    @Override
    public String toString() {
        if (tags.isEmpty())
            return "Unknown " + cap;
        else
            return "Unknown " + tags.toString() + " " + cap;
    }

    @Override
    public String stringValue() {
        return "";
    }

    public String getRaw() {
        return cap;
    }

    public String getUser() {
        return user;
    }
}
