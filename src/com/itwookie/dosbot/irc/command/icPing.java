package com.itwookie.dosbot.irc.command;

import com.itwookie.dosbot.irc.IRCMessage;

import java.util.Optional;

public class icPing implements AbstractCommand {

    private String server;

    public icPing(String server) {
        this.server = server;
    }

    public icPing(IRCMessage msg) {
        server = msg.getParams().split(" ")[0];
        netraw = msg;
    }

    private IRCMessage netraw = null;

    @Override
    public Optional<IRCMessage> getMessageRaw() {
        return Optional.ofNullable(netraw);
    }

    @Override
    public String toString() {
        return "Ping from " + server;
    }

    @Override
    public String stringValue() {
        return "PING " + server;
    }

    public String getServer() {
        return server;
    }
}
