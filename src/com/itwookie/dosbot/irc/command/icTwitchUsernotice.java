package com.itwookie.dosbot.irc.command;

import com.itwookie.dosbot.irc.IRCMessage;
import com.itwookie.dosbot.irc.TagBag;

import java.util.Optional;

/**
 * Anounce events to channel, can't be sent
 */
public class icTwitchUsernotice implements AbstractCommand {

    private TagBag tags;
    private String channel, message;
    private String user;

    public icTwitchUsernotice(IRCMessage msg) {
        tags = msg.getTags();
        { //in case no second argument exists there needs to be some logic
            String rawparams = msg.getParams();
            int off;
            if ((off = rawparams.indexOf(" :")) >= 0) {
                channel = rawparams.substring(0, off);
                message = rawparams.substring(off + 2);
            } else {
                channel = rawparams;
                message = "";
            }
        }
        user = msg.getUser();
        netraw = msg;
    }

    private IRCMessage netraw = null;

    @Override
    public Optional<IRCMessage> getMessageRaw() {
        return Optional.ofNullable(netraw);
    }

    @Override
    public String toString() {
        return "Usernotice: " + tags.getOrDefault("system-msg", tags.toString());
    }

    @Override
    public String stringValue() {
        return "";
    }

    public String getChannel() {
        return channel;
    }

    public TagBag getTags() {
        return tags;
    }

    public String getUser() {
        return user;
    }

    public String getMessage() {
        return message;
    }
}
