package com.itwookie.dosbot.irc.command;

import com.itwookie.dosbot.irc.IRCMessage;

import java.util.Optional;

/**
 * requires a IRCMessage constructor and registration to IRCMessage
 */
public interface AbstractCommand {
    /**
     * @return readable representation
     */
    String toString();

    /**
     * The promise of this is to generate a executable command string,
     * not an exact replica of construction data or received message.
     *
     * @return networkable string
     */
    String stringValue();

    /**
     * return the IRCMessage object if the command was constructed by
     * a received irc message
     */
    Optional<IRCMessage> getMessageRaw();
}
