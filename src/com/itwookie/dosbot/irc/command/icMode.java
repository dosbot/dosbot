package com.itwookie.dosbot.irc.command;

import com.itwookie.dosbot.irc.IRCMessage;

import java.util.Optional;

public class icMode implements AbstractCommand {

    private String user;
    private boolean operator;

    public icMode(String user, boolean operator) {
        this.user = user;
        this.operator = operator;
    }

    public icMode(IRCMessage msg) {
        String[] args = msg.getParams().split(" ");
        user = args[2];
        operator = args[1].equals("+o");
        netraw = msg;
    }

    private IRCMessage netraw = null;

    @Override
    public Optional<IRCMessage> getMessageRaw() {
        return Optional.ofNullable(netraw);
    }

    @Override
    public String toString() {
        return (operator ? "Opped: " : "Deopped: ") + user;
    }

    @Override
    public String stringValue() {
        return "MODE " + user + " " + (operator ? "+o" : "-o");
    }


    public String getUser() {
        return user;
    }

    public boolean isOperator() {
        return operator;
    }
}
