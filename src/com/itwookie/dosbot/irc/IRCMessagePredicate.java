package com.itwookie.dosbot.irc;

import java.util.Objects;
import java.util.function.Predicate;

/**
 * Testing a raw string as irc message
 */
@FunctionalInterface
public interface IRCMessagePredicate extends Predicate<IRCMessage> {
    boolean test(IRCMessage t);

    default IRCMessageConsumer then(IRCMessageConsumer consumer) {
        Objects.requireNonNull(consumer);
        return (m) -> {
            if (test(m)) consumer.accept(m);
        };
    }

    /**
     * accepting predicate checking the IRCMessage instead of the raw string
     */
    default IRCMessagePredicate and2(Predicate<IRCMessage> other) {
        Objects.requireNonNull(other);
        return (m) -> test(m) && other.test(m);
    }

    /**
     * accepting predicate checking the IRCMessage instead of the raw string
     */
    default IRCMessagePredicate or2(Predicate<IRCMessage> other) {
        Objects.requireNonNull(other);
        return (m) -> test(m) || other.test(m);
    }
}
