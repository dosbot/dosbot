package com.itwookie.dosbot.irc;

import com.itwookie.dosbot.irc.command.AbstractCommand;

import java.util.Objects;
import java.util.function.Predicate;

/**
 * Testing a raw string as command
 */
@FunctionalInterface
public interface CommandPredicate extends Predicate<IRCMessage> {
    default boolean test(IRCMessage s) {
        return test(s.toCommand());
    }

    boolean test(AbstractCommand t);

    default CommandConsumer then(CommandConsumer consumer) {
        Objects.requireNonNull(consumer);
        return (m) -> {
            if (test(m)) consumer.accept(m);
        };
    }

    /**
     * accepting predicate checking the IRCMessage instead of the raw string
     */
    default CommandPredicate and2(Predicate<AbstractCommand> other) {
        Objects.requireNonNull(other);
        return (m) -> test(m) && other.test(m);
    }

    /**
     * accepting predicate checking the IRCMessage instead of the raw string
     */
    default CommandPredicate or2(Predicate<AbstractCommand> other) {
        Objects.requireNonNull(other);
        return (m) -> test(m) || other.test(m);
    }
}
