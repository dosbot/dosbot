package com.itwookie.dosbot.irc;

import java.util.function.Consumer;

/**
 * Consuming a raw string as irc message
 */
@FunctionalInterface
public interface IRCMessageConsumer extends Consumer<IRCMessage> {
    void accept(IRCMessage t);
}
