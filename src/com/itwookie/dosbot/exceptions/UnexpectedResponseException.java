package com.itwookie.dosbot.exceptions;

import com.itwookie.dosbot.irc.IRCMessage;
import com.itwookie.dosbot.irc.IRCMessageException;

public class UnexpectedResponseException extends IRCMessageException {
    public UnexpectedResponseException(String message, IRCMessage response) {
        super(message, response);
    }
}
