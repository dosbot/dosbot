package com.itwookie.dosbot.exceptions;

import java.io.IOException;

public class ConnectException extends IOException {
    public ConnectException() {
    }

    public ConnectException(String message) {
        super(message);
    }

    public ConnectException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConnectException(Throwable cause) {
        super(cause);
    }
}
