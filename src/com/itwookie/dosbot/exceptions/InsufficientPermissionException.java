package com.itwookie.dosbot.exceptions;

/**
 * thrown when a command on a channel or user instance requires higher status (mod/broadcaster)
 */
public class InsufficientPermissionException extends RuntimeException {
    public InsufficientPermissionException() {
    }

    public InsufficientPermissionException(String message) {
        super(message);
    }

    public InsufficientPermissionException(String message, Throwable cause) {
        super(message, cause);
    }

    public InsufficientPermissionException(Throwable cause) {
        super(cause);
    }

    public InsufficientPermissionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
