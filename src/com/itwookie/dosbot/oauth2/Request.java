package com.itwookie.dosbot.oauth2;

import java.io.IOException;
import java.net.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Request {
    private URL url;
    private String requestMethod;

    public Request(String url) throws MalformedURLException {
        this.url = new URL(url);
        this.requestMethod = "GET";
    }

    public Request(String method, String url) throws MalformedURLException {
        this.url = new URL(url);
        this.requestMethod = method;
    }

    public Request(URL url) {
        this.url = url;
        this.requestMethod = "GET";
    }

    public Request(String method, URL url) {
        this.url = url;
        this.requestMethod = method;
    }

    private Map<String, String> requestHeaders = new HashMap<String, String>();

    public void setRequestHeader(String header, String value) {
        requestHeaders.put(header, value);
    }

    private Map<String, String> parameters = new HashMap<String, String>();

    public void setParameter(String key, String value) {
        parameters.put(key, value);
    }

    /**
     * get a http/https URL connection with all data represented by this request set, but nothing more.
     * e.g. timeouts are untouched and setDoInput have to be called if necessary
     */
    URLConnection getRawConnection() throws IOException, URISyntaxException {
        URL iurl;
        if (requestMethod.equalsIgnoreCase("GET")) {
            //squish query params
            String oldParams = url.getQuery();
            String params = getParameterURLencoded();
            if (oldParams == null) ; /* don't change params */
            else if (params == null) params = oldParams;
            else params = params + '&' + oldParams;
            //rebuild url with params
            iurl = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), params, url.getRef()).toURL();
        } else {
            iurl = url;
        }
        HttpURLConnection connection = (HttpURLConnection) iurl.openConnection();
        connection.setRequestMethod(requestMethod);
        requestHeaders.put("User-Agent", "DosBot/1.0 (Java/10) by twitch.tv/DosMike");
        if (requestMethod.equalsIgnoreCase("POST")) {
            requestHeaders.put("Content-Type", "application/x-www-form-urlencoded");
            requestHeaders.put("Content-Length", String.valueOf(getParameterURLencoded().getBytes().length));
            connection.setDoOutput(true);
        }
        for (Map.Entry<String, String> header : requestHeaders.entrySet())
            connection.setRequestProperty(header.getKey(), header.getValue());
        return connection;
    }

    /**
     * this will open the connection if not already opened!
     */
    void writeRequestBody(HttpURLConnection connection) throws IOException {
        connection.getOutputStream().write(getParameterURLencoded().getBytes());
        connection.getOutputStream().flush();
    }

    /**
     * @return one string containing params or null
     */
    public String getParameterURLencoded() {
        if (!parameters.isEmpty()) {
            StringBuffer sb = new StringBuffer(1024);
            for (Map.Entry<String, String> data : parameters.entrySet()) {
                if (sb.length() == 0) {
                    sb.append(URLEncoder.encode(data.getKey()));
                    sb.append('=');
                    sb.append(URLEncoder.encode(data.getValue()));
                } else {
                    sb.append('&');
                    sb.append(URLEncoder.encode(data.getKey()));
                    sb.append('=');
                    sb.append(URLEncoder.encode(data.getValue()));
                }
            }
            return sb.toString();
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Request request = (Request) o;
        return Objects.equals(url, request.url) &&
                Objects.equals(requestMethod, request.requestMethod) &&
                Objects.equals(requestHeaders, request.requestHeaders) &&
                Objects.equals(parameters, request.parameters);
    }

    @Override
    public int hashCode() {
        return Objects.hash(url, requestMethod, requestHeaders, parameters);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> headers : requestHeaders.entrySet()) {
            sb.append(headers.getKey());
            sb.append(": ");
            sb.append(headers.getValue());
            sb.append("\n");
        }
        String rp = getParameterURLencoded();
        if (rp != null)
            return requestMethod + " " + url.toString() + "\nHeader: \n" + sb.toString() + "Params: \n" + String.join("\n", getParameterURLencoded().split("&"));
        else
            return requestMethod + " " + url.toString() + "\nHeader: \n" + sb.toString() + "Params: ";
    }
}
