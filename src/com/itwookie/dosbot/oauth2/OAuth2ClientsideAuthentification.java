package com.itwookie.dosbot.oauth2;

import org.json.JSONObject;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.Base64;

public final class OAuth2ClientsideAuthentification extends OAuth2 {

    OAuth2ClientsideAuthentification(OAuthConfiguration configuration) {
        super(configuration);
    }

    public final void auth() {
        //can't refresh token, none provided
        String webResponse = verifyUser();
        if (webResponse != null)
            authFetchToken(webResponse);
    }

    /**
     * @return the oauth url containing the one-time authorization code
     */
    final String verifyUser() {
        responseServer = new OAuthWebServerHTTP(this, cfg.getRedirectURL());
        responseServer.start();
        byte[] bytes = new byte[16];
        random.nextBytes(bytes);
        STATE = Base64.getEncoder().encodeToString(bytes);
        try {
            URI request = new URI(cfg.getAuthULR() +
                    "?client_id=" + cfg.i() +
                    "&redirect_uri=" + URLEncoder.encode(cfg.getRedirectURL()) +
                    "&response_type=token" +
                    "&scope=" + URLEncoder.encode(String.join(" ", cfg.getScopes())) +
                    "&force_verify=false" +
                    "&state=" + URLEncoder.encode(STATE));
            if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
                Desktop.getDesktop().browse(request);
            } else {
                log.log("Please navigate to " + request.toString());
            }
        } catch (URISyntaxException ignore) {
            /* shouldn't be malformed */
            ignore.printStackTrace();
            return null;
        } catch (IOException e) {
            Message("Could not open Browser", JOptionPane.ERROR_MESSAGE);
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        while (!responseServer.getResponse().isPresent()) {
            if (!responseServer.running) break;
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {

            }
        }
        return responseServer.getResponse().orElse(null);
    }

    final void authFetchToken(String data) {
        accessToken = new JSONObject(data);
    }

    final HttpURLConnection getAuthorizedConnection(Request request) {
        if (accessToken == null) throw new IllegalStateException("Missing initial authorization");
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) request.getRawConnection();
            connection.setRequestProperty("Authorization", "Bearer " + accessToken.getString("access_token")); //for API5: OAuth <TOKEN>
            connection.setRequestProperty("Client-ID", cfg.i());
        } catch (URISyntaxException i) {
            /**/
            i.printStackTrace();
        } catch (IOException exception) {
            Message("Could not establish connection: " + exception.getMessage(), JOptionPane.ERROR_MESSAGE);
            exception.printStackTrace();
        }
        return connection;
    }
}
