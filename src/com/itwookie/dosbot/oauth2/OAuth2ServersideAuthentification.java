package com.itwookie.dosbot.oauth2;

import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public final class OAuth2ServersideAuthentification extends OAuth2 {

    OAuth2ServersideAuthentification(OAuthConfiguration configuration) {
        super(configuration);
    }

    public final void auth() {
        if (accessToken == null) {
            String codeURI = verifyUser();
            if (codeURI != null)
                authFetchToken(codeURI);
        } else {
            if (!authRefresh())
                auth(); //retry with reset accessToken
        }
    }

    /**
     * @return the oauth url containing the one-time authorization code
     */
    final String verifyUser() {
        responseServer = new OAuthWebServerHTTP(this, cfg.getRedirectURL());
        responseServer.start();
        byte[] bytes = new byte[16];
        random.nextBytes(bytes);
        STATE = Base64.getEncoder().encodeToString(bytes);
        try {
            URI request = new URI(cfg.getAuthULR() +
                    "?client_id=" + cfg.i() +
                    "&redirect_uri=" + URLEncoder.encode(cfg.getRedirectURL()) +
                    "&response_type=code" +
                    "&scope=" + URLEncoder.encode(String.join(" ", cfg.getScopes())) +
                    "&force_verify=false" +
                    "&state=" + URLEncoder.encode(STATE));
            if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
                Desktop.getDesktop().browse(request);
            } else {
                log.log("Please navigate to " + request.toString());
            }
        } catch (URISyntaxException ignore) {
            /* shouldn't be malformed */
            ignore.printStackTrace();
            return null;
        } catch (IOException e) {
            Message("Could not open Browser", JOptionPane.ERROR_MESSAGE);
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        while (!responseServer.getResponse().isPresent()) {
            if (!responseServer.running) break;
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {

            }
        }
        return responseServer.getResponse().orElse(null);
    }

    final void authFetchToken(String url) {
        Map<String, String> params = new HashMap<>();

        //check if response was valid and not tinkered with
        try {
            if (pendingPane != null) pendingPane.dispose();
            {
                URI uri;
                try {
                    uri = new URI(url);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(1);
                    return;
                }

                String qs = uri.getQuery();
                if (qs == null) throw new RuntimeException("No auth?");
                String[] ss = qs.split("&");
                for (String s : ss) {
                    String key = s.substring(0, s.indexOf('='));
                    String value = s.substring(key.length() + 1);
                    params.put(key, value);
                }
            }
            Map<String, String> origparam = new HashMap<>(); //predefined params that have to be returned
            {
                URI uri;
                try {
                    uri = new URI(cfg.getRedirectURL());
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }

                String[] ss = uri.getQuery().split("&");
                for (String s : ss) {
                    String key = s.substring(0, s.indexOf('='));
                    String value = s.substring(key.length() + 1);
                    origparam.put(key, value);
                }
            }

            for (String key : origparam.keySet()) {
                if (!params.containsKey(key) || !origparam.get(key).equals(params.get(key))) {
                    throw new IllegalStateException("Required Parameters missing");
                }
            }
            if (!params.get("state").equals(STATE)) {
                log.err(STATE);
                log.err(params.get("state"));
                throw new IllegalStateException("State mismatch");
            }
            if (params.containsKey("error")) {
                Message("Authorization declined!\n\nThe Bot will now terminate.", JOptionPane.INFORMATION_MESSAGE);
                System.exit(0);
                return;
            }
            if (!params.get("scope").replace('+', ' ').equalsIgnoreCase(String.join(" ", cfg.getScopes()))) {
                log.err(String.join(" ", cfg.getScopes()));
                log.err(params.get("scope").replace('+', ' '));
                throw new IllegalStateException("Scope changed");
            }

            //Use code for further login
        } catch (IllegalStateException e) {
            Message(e.getMessage(), JOptionPane.ERROR_MESSAGE);
            return;
        }
        //response seems good, use one-time code to fetch access token

        try {
            Request request = new Request("POST", cfg.getTokenURL());
            request.setParameter("client_id", cfg.i());
            request.setParameter("client_secret", cfg.s());
            request.setParameter("code", params.get("code"));
            request.setParameter("grant_type", "authorization_code");
            request.setParameter("redirect_uri", cfg.getRedirectURL());
            HttpsURLConnection connection = (HttpsURLConnection) request.getRawConnection();
            connection.setReadTimeout(5000);
            connection.setDoInput(true);

            connection.connect();
            request.writeRequestBody(connection);

            if (connection.getResponseCode() != 200) {
                Message("Connection Refused: " + connection.getResponseCode() + " " + connection.getResponseMessage(), JOptionPane.ERROR_MESSAGE);
                return;
            }
            StringBuffer sb = new StringBuffer(1024);
            InputStream in = connection.getInputStream();
            int c;
            while ((c = in.read()) >= 0) sb.append((char) c);

            accessToken = new JSONObject(sb.toString());
//            lastValidMS = System.currentTimeMillis() + accessToken.getLong("expires_in")*1000;

            connection.disconnect();
        } catch (MalformedURLException | URISyntaxException ignore) {
            //should not happen
            ignore.printStackTrace();
        } catch (IOException e) {
            Message(e.getMessage(), JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }

    final boolean authRefresh() {
        try {
            Request request = new Request("POST", cfg.getTokenURL());
            request.setParameter("client_id", cfg.i());
            request.setParameter("client_secret", cfg.s());
            request.setParameter("refresh_token", accessToken.getString("refresh_token"));
            request.setParameter("grant_type", "refresh_token");
            HttpsURLConnection connection = (HttpsURLConnection) request.getRawConnection();
            connection.setReadTimeout(5000);
            connection.setDoInput(true);

            connection.connect();
            request.writeRequestBody(connection);

            if (connection.getResponseCode() != 200) {
//                Message("Unable to renew Access Token!\n\nPlease re-authenticate manually", JOptionPane.INFORMATION_MESSAGE);
                accessToken = null;
                return false;
            }
            StringBuffer sb = new StringBuffer(1024);
            InputStream in = connection.getInputStream();
            int c;
            while ((c = in.read()) >= 0) sb.append((char) c);

            accessToken = new JSONObject(sb.toString());

            connection.disconnect();
        } catch (MalformedURLException | URISyntaxException ignore) {
            //should not happen
            ignore.printStackTrace();
            return false;
        } catch (IOException e) {
            Message(e.getMessage(), JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
            return false;
        }
        return true;
    }

    final HttpURLConnection getAuthorizedConnection(Request request) {
        if (accessToken == null) throw new IllegalStateException("Missing initial authorization");
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) request.getRawConnection();
            connection.setRequestProperty("Authorization", "Bearer " + accessToken.getString("access_token")); //for API5: OAuth <TOKEN>
            connection.setRequestProperty("Client-ID", cfg.i());
        } catch (URISyntaxException i) {
            /**/
            i.printStackTrace();
        } catch (IOException exception) {
            Message("Could not establish connection: " + exception.getMessage(), JOptionPane.ERROR_MESSAGE);
            exception.printStackTrace();
        }
        return connection;
    }
}
