package com.itwookie.dosbot.oauth2;

import com.itwookie.dosbot.api.Scopes;
import com.itwookie.logger.Logger;
import org.intellij.lang.annotations.MagicConstant;
import org.json.JSONException;
import org.json.JSONObject;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Random;
import java.util.function.Consumer;

public abstract class OAuth2 {
    protected final OAuthConfiguration cfg;

    OAuth2(OAuthConfiguration configuration) {
        cfg = configuration;
    }

    protected Logger log = Logger.getLogger();

    protected JSONObject accessToken = null;

    protected String STATE = null;
    protected static Random random = new Random(System.currentTimeMillis());
    protected OAuthWebServerHTTP responseServer = null;

    public abstract void auth();

    public final String getCurrentAccessToken() {
        if (accessToken == null) return null;
        try {
            return accessToken.getString("access_token");
        } catch (JSONException ignore) {
            return null;
        }
    }

    public final boolean hasScope(@MagicConstant(valuesFromClass = Scopes.class) String scopeName) {
        for (String scope : cfg.getScopes()) {
            if (scope.equalsIgnoreCase(scopeName))
                return true;
        }
        return false;
    }


    static final void Message(String msg, int JOptionType) {
        JOptionPane.showMessageDialog(null, msg, "DosBot - OAuth2", JOptionType);
    }

    JDialog pendingPane;

    final JDialog createPendingPane() {
        JOptionPane jop = new JOptionPane("Waiting for " + cfg.getHost() + "\nPlease click cancel if the authentication failed.", JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{"Cancel"});
        pendingPane = jop.createDialog("OAuth2 - Pending Authentication");
        pendingPane.setModalityType(Dialog.ModalityType.MODELESS);
        pendingPane.setVisible(true);
        return pendingPane;
    }

    abstract HttpURLConnection getAuthorizedConnection(Request request);

    /**
     * This method will automatically authorize the OAuth connection, write the request and call upon requestTransformer
     * to set missing connection parameters (might be called multiple times) and returns a HttpURLConnection that contains
     * the response and should never be 401 (Unauthorized).
     *
     * @param base               specify all base properties for a request
     * @param requestTransformer a consumer that might configure a https url connection before it's opened <br><b>NOTE: DO NOT OPEN THE CONNECTION OR CALL METHODS THAT DO SO!</b>
     * @return a HttpURLConnection that's authorized with user knowledge
     * @throws IllegalStateException if the user could not be re-authorized.
     */
    public final HttpURLConnection request(Request base, Consumer<HttpURLConnection> requestTransformer) throws IOException, IllegalStateException {
        HttpURLConnection connection = getAuthorizedConnection(base);
        requestTransformer.accept(connection);
        if (connection.getResponseCode() == 401) {
            //re-auth
            auth();
            //retry
            connection = getAuthorizedConnection(base);
            requestTransformer.accept(connection);
            if (connection.getResponseCode() == 401)
                throw new IllegalStateException("Unable to re-authorize");
        }
        return connection;
    }
}
