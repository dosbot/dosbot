package com.itwookie.dosbot.oauth2;

import com.itwookie.logger.Logger;

import javax.swing.*;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.util.Optional;

public final class OAuthWebServerHTTP extends Thread {

    private ServerSocket tcpServer;
    private OAuth2 authParent;
    private URI uri;

    OAuthWebServerHTTP(OAuth2 authParent, String redirectURI) {
        setName("OAuth Web Server");
        this.authParent = authParent;
        try {
            uri = new URI(redirectURI);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        int port = uri.getPort();
        try {
            tcpServer = new ServerSocket(port > 0 ? port : 80);
            tcpServer.setSoTimeout(1000);
        } catch (Exception e) {
            e.printStackTrace();
            OAuth2.Message(String.format("Could not create listener on port %d :\n\n%s", (port > 0 ? port : 80), e.getMessage()), JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
    }

    boolean running = true, success = false;

    public void cancel() {
        running = false;
    }

    private String response = null;
    private final Object resultMutex = new Object();

    public final Optional<String> getResponse() {
        synchronized (resultMutex) {
            return Optional.ofNullable(response);
        }
    }

    JDialog pendingPane = null;

    @Override
    public final void run() {
        while (running && !success) {
            if (pendingPane == null) {
                pendingPane = authParent.createPendingPane();
            } else if (!pendingPane.isShowing()) {
                running = false;
                pendingPane.dispose();
            }
            try {
                Socket client = tcpServer.accept();
                client.setSoTimeout(5000);
                client.setKeepAlive(true);

                // read data requested by web browser
                InputStream in = client.getInputStream();
                StringBuffer sb = new StringBuffer(1024);
                int c;
                while ((c = in.read()) >= 0) {
                    sb.append((char) c);
                    //checking for header terminator
                    if ((sb.length() > 4 && sb.substring(sb.length() - 4, sb.length()).equals("\r\n\r\n"))) {
                        break;
                    }
                }

                boolean error = false, isRequestToIndex = false;
                int contentLength = 0;
                String requestedUriString;
                URI requestedUri;
                String[] header;
                {
                    String tmp = sb.toString(); //EVERYTHING
                    header = tmp.split("\r\n"); //GET url HTTP/1.1
                    tmp = header[0];
                    if (!tmp.contains(" ")) Logger.getLogger("OAuth").err(tmp);
                    tmp = tmp.split(" ")[1]; //url
//                    System.out.println(tmp);
                    error = tmp.contains("&error=");
                    requestedUriString = tmp;

                    requestedUri = URI.create(tmp);
                    String path1 = requestedUri.getPath();
                    if (path1.startsWith("/")) path1 = path1.substring(1);
                    String path2 = uri.getPath();
                    if (path2.startsWith("/")) path2 = path1.substring(1);
                    isRequestToIndex = path1.equalsIgnoreCase(path2);
                }
                for (String head : header) {
                    if (head.toLowerCase().startsWith("content-length:")) {
                        contentLength = Integer.parseInt(head.split(": ")[1].trim());
                    }
                }

                if (!error && contentLength > 0) {

                    boolean hasData = false;
                    for (String s : header) {
                        if (s.toLowerCase().startsWith("x-auth-confirm:")) hasData = true;
                    }
                    if (hasData) { //read rest
                        Logger.getLogger("OAuth").log("Client flow result received");
                        sb.setLength(0);
                        for (int i = 0; i < contentLength && (c = in.read()) >= 0; i++) {
                            sb.append((char) c);
                        }
                        success = true;
                        synchronized (resultMutex) {
                            response = sb.toString();
                        }
                    } else
                        Logger.getLogger("OAuth").log("Rejected unknown post request");

                } else {
                    if (!isRequestToIndex) { // requesting images, etc
                        Logger.getLogger("OAuth").log("Rejected Resource " + requestedUri.getPath());
                        OutputStream out = client.getOutputStream();
                        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));
                        //write HTTP-headers
                        bw.write("HTTP/1.1 403 Forbidden\r\n");
                        bw.write("content-length: 0\r\n");
                        bw.write("content-type: text/html\r\n");
                        bw.write("\r\n"); //header terminator
                        bw.flush();
                        bw.close();
                    } else {
                        if (!uri.getQuery().equalsIgnoreCase(requestedUri.getQuery())) {
                            //if we have more data in the requested url than specified in the
                            //redirect url we got the data!
                            Logger.getLogger("OAuth").log("Server flow result received");

                            success = true;
                            synchronized (resultMutex) {
                                response = requestedUriString;
                            }

                        } else
                            //otherwise we have to wait for a post request
                            Logger.getLogger("OAuth").log("Client flow temporary page");

                        //write nice success page to browsette
                        in = ClassLoader.getSystemResourceAsStream(error ? "oauth_declined.htm" : "oauth_success.htm");
                        sb = new StringBuffer(1024);
                        while ((c = in.read()) >= 0)
                            sb.append((char) c);
                        in.close();

                        OutputStream out = client.getOutputStream();
                        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));
                        //write HTTP-headers
                        bw.write("HTTP/1.1 200 OK\r\n");
                        bw.write(String.format("content-length: %d\r\n", sb.length()));
                        bw.write("content-type: text/html\r\n");
                        bw.write("\r\n"); //header terminator
                        bw.write(sb.toString());
                        bw.flush();
                        bw.close();
                    }
                }

                client.close();
            } catch (SocketTimeoutException ignore) {
                /* retry */
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        pendingPane.dispose();
    }
}
