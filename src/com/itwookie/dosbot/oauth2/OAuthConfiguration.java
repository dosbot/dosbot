package com.itwookie.dosbot.oauth2;

import java.net.URL;
import java.util.Base64;
import java.util.Objects;

public final class OAuthConfiguration {

    public enum Authorization {ServerSideApplication, ClientSideApplication}

    private String ai, as, authULR, tokenURL, redirectURL, authHost;
    private String[] scopes;

    private OAuthConfiguration() {

    }

    public static final class Builder {
        OAuthConfiguration cfg;
        int valid = 0;
        Authorization type = null;

        private Builder() {
            cfg = new OAuthConfiguration();
        }

        /**
         * for server-side applications taking client id and secret
         */
        public final Builder auth(String i, String s) {
            Objects.requireNonNull(i);
            Objects.requireNonNull(s);
            cfg.ai = new String(Base64.getDecoder().decode(i));
            cfg.as = new String(Base64.getDecoder().decode(s));
            valid |= 0b000001;
            type = Authorization.ServerSideApplication;
            return Builder.this;
        }

        /**
         * for client-side applications only taking client id
         */
        public final Builder auth(String i) {
            Objects.requireNonNull(i);
            cfg.ai = new String(Base64.getDecoder().decode(i));
            valid |= 0b000001;
            type = Authorization.ClientSideApplication;
            return Builder.this;
        }

        public final Builder scopes(String... scopes) {
            Objects.requireNonNull(scopes);
            if (scopes.length == 0)
                throw new IllegalArgumentException("You probably do not intend to do nothing with the API, you forgot to specify the scopes.");
            cfg.scopes = scopes;
            valid |= 0b000010;
            return Builder.this;
        }

        /**
         * @param authURL the url to redirect the user to for visual authorization.<br>The URL may not contain parameters!
         */
        public final Builder authURL(URL authURL) {
            Objects.requireNonNull(authURL);
            if (authURL.getQuery() != null || authURL.getRef() != null)
                throw new IllegalArgumentException("Malformed URL");
            cfg.authULR = authURL.toString();
            cfg.authHost = authURL.getHost();
            valid |= 0b000100;
            return Builder.this;
        }

        /**
         * @param tokenURL the url to fetch and update the login token.<br>The URL may not contain parameters!
         */
        public final Builder tokenURL(URL tokenURL) {
            Objects.requireNonNull(tokenURL);
            if (tokenURL.getQuery() != null || tokenURL.getRef() != null)
                throw new IllegalArgumentException("Malformed URL");
            cfg.tokenURL = tokenURL.toString();
            valid |= 0b001000;
            return Builder.this;
        }

        /**
         * @param redirectURL the target url the auth page shall redirect the user to.<br><b>Remember that this has to match the specified URL exactly</b>
         */
        public final Builder redirectURL(String redirectURL) {
            Objects.requireNonNull(redirectURL);
            cfg.redirectURL = redirectURL;
            valid |= 0b010000;
            return Builder.this;
        }

        public final OAuth2 build() {
            if (valid != 0b011111 || type == null) throw new IllegalStateException();

            return (type == Authorization.ServerSideApplication)
                    ? new OAuth2ServersideAuthentification(cfg)
                    : new OAuth2ClientsideAuthentification(cfg);
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    final String i() {
        return ai;
    }

    final String s() {
        return as;
    }

    final String getAuthULR() {
        return authULR;
    }

    final String getTokenURL() {
        return tokenURL;
    }

    final String getRedirectURL() {
        return redirectURL;
    }

    final String[] getScopes() {
        return scopes;
    }

    final String getHost() {
        return authHost;
    }
}
