package com.itwookie.dosbot.events.chat;

import com.itwookie.dosbot.events.CancelableEvent;
import com.itwookie.dosbot.twitch.User;
import org.jetbrains.annotations.NotNull;

public class UserCommandEvent extends CancelableEvent {

    private User sender;
    private String command;
    private String arumentstring;

    public UserCommandEvent(@NotNull User sender, @NotNull String message) {
        this.sender = sender;
        if (message.indexOf(' ') > 1) {
            this.command = message.substring(1, message.indexOf(' '));
            this.arumentstring = message.substring(this.command.length() + 1);
        } else {
            this.command = message.substring(1);
            this.arumentstring = "";
        }
    }

    public User getSender() {
        return sender;
    }

    /**
     * get the name for the chat command
     */
    public String getCommandName() {
        return command;
    }

    /**
     * returns the joined argument string for this chat command
     */
    public String getArumentString() {
        return arumentstring;
    }
}
