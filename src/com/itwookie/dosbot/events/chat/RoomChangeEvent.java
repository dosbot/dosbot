package com.itwookie.dosbot.events.chat;

import com.itwookie.dosbot.TwitchClient;
import com.itwookie.dosbot.events.Event;
import com.itwookie.dosbot.irc.TagBag;
import com.itwookie.dosbot.twitch.Channel;
import com.itwookie.utils.Transition;

import java.util.Locale;

/**
 * issued whenever the room changes AKA a ROOMSTATE message was received.
 * This event is called just before the channel updates, so you can compare changes
 */
public class RoomChangeEvent implements Event {

    private Transition<Locale> language;
    private Transition<Boolean> emoteOnly;
    private Transition<Integer> followerOnly;
    private Transition<Boolean> r9k;
    private Transition<Integer> slow;
    private Transition<Boolean> subOnly;

    public RoomChangeEvent(Locale language, boolean emoteOnly, int followerOnly, boolean r9k, int slow, boolean subOnly) {
        Channel channel = TwitchClient.getChannel();
        this.language = new Transition<>(channel.getBroadcasterLang().orElse(null), language);
        this.emoteOnly = new Transition<>(channel.isEmoteOnly(), emoteOnly);
        this.followerOnly = new Transition<>(channel.getFollowerOnly().orElse(-1), followerOnly);
        this.r9k = new Transition<>(channel.isR9k(), r9k);
        this.slow = new Transition<>(channel.getSlow(), slow);
        this.subOnly = new Transition<>(channel.isSubsOnly(), subOnly);
    }

    public static RoomChangeEvent fromTags(TagBag tags) {
        Locale broadcasterLang;
        boolean emoteOnly;
        int followerOnly;
        boolean r9k;
        int slow;
        boolean subsOnly;

        String tmp = tags.getOrDefault("broadcaster-lang", "");
        if (tmp.isEmpty()) broadcasterLang = null;
        else broadcasterLang = Locale.forLanguageTag(tmp);
        emoteOnly = s2b(tags.getOrDefault("emote-only", "0"));
        followerOnly = s2i(tags.getOrDefault("followers-only", "-1"));
        r9k = s2b(tags.getOrDefault("r9k", "0"));
        slow = Math.max(0, s2i(tags.getOrDefault("slow", "0")));
        subsOnly = s2b(tags.getOrDefault("subs-only", "0"));

        return new RoomChangeEvent(
                broadcasterLang,
                emoteOnly,
                followerOnly,
                r9k,
                slow,
                subsOnly
        );
    }

    private static boolean s2b(String s) {
        if (s.isEmpty()) return false;
        return s.equals("1");
    }

    private static int s2i(String s) {
        if (s.isEmpty()) return 0;
        try {
            return Integer.valueOf(s);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    /**
     * @return changes in the broadcast language
     */
    public Transition<Locale> getLanguage() {
        return language;
    }

    /**
     * @return cahnges in emote only mode
     */
    public Transition<Boolean> getEmoteOnly() {
        return emoteOnly;
    }

    /**
     * @return changes in follower only mode
     */
    public Transition<Integer> getFollowerOnly() {
        return followerOnly;
    }

    /**
     * @return changes in r9k mode
     */
    public Transition<Boolean> getR9k() {
        return r9k;
    }

    /**
     * @return changes in slow mode
     */
    public Transition<Integer> getSlow() {
        return slow;
    }

    /**
     * @return changes in sub only mode
     */
    public Transition<Boolean> getSubOnly() {
        return subOnly;
    }

}
