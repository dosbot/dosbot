package com.itwookie.dosbot.events.chat;

import com.itwookie.dosbot.events.Event;
import com.itwookie.dosbot.irc.TagBag;
import com.itwookie.dosbot.spanned.SpannedString;
import com.itwookie.dosbot.twitch.OfflineUser;

import java.util.UUID;

public class UserNoticeEvent implements Event {

    public enum MessageType {UNKNOWN, SUB, RESUB, SUBGIFT, RAID, RITUAL}

    public enum SubPlan {
        Prime, $1000, $2000, $3000;

        public static SubPlan fromString(String s) {
            if (s.equalsIgnoreCase("prime"))
                return Prime;
            if (s.equals("1000"))
                return $1000;
            if (s.equals("2000"))
                return $2000;
            if (s.equals("3000"))
                return $3000;
            throw new IllegalArgumentException("Unknown sub plan");
        }
    }

    //private User sender;
    private SpannedString message;
    private String systemMsg;
    private UUID messageID;
    private OfflineUser user;
    protected MessageType type = MessageType.UNKNOWN;

    protected UserNoticeEvent(SpannedString message, TagBag tags) {
        this.user = new OfflineUser(tags);
        this.message = message;
        this.systemMsg = tags.get("system-msg");
        try {
            this.messageID = UUID.fromString(tags.get("id"));
        } catch (IllegalArgumentException e) {
            this.messageID = new UUID(0l, 0l);
        }
    }

    /**
     * This event originates from a USERNOTICE. this twitch IRC message contains userdata in the tags bag.<br>
     * Since this does not necessarily mean the user has to be in the channel this event returns a offline user.<br>
     * You can easily check if the user is only by calling <code>getUser().getOnline().isPresent()</code><br>
     * Subgifts and Raid have a special target, check getTarget() or getSource() respectively.
     */
    public OfflineUser getUser() {
        return user;
    }

    public SpannedString getMessage() {
        return message;
    }

    public String getSystemMsg() {
        return systemMsg;
    }

    public UUID getMessageID() {
        return messageID;
    }

    public MessageType getType() {
        return type;
    }

    public static class Sub extends UserNoticeEvent {
        private String planName;
        private int months;
        private SubPlan plan;

        protected Sub(SpannedString message, TagBag tags) {
            super(message, tags);
            type = MessageType.SUB;
            planName = tags.getOrDefault("msg-param-sub-plan-name", "??");
            plan = SubPlan.fromString(tags.get("msg-param-sub-plan"));
            months = 0;
        }

        public String getPlanName() {
            return planName;
        }

        public int getMonths() {
            return months;
        }

        public SubPlan getPlan() {
            return plan;
        }
    }

    public static class Resub extends UserNoticeEvent {
        private String planName;
        private int months;
        private SubPlan plan;

        protected Resub(SpannedString message, TagBag tags) {
            super(message, tags);
            type = MessageType.RESUB;
            planName = tags.getOrDefault("msg-param-sub-plan-name", "??");
            plan = SubPlan.fromString(tags.get("msg-param-sub-plan"));
            months = Integer.parseInt(tags.getOrDefault("msg-param-months", "0"));
        }

        public String getPlanName() {
            return planName;
        }

        public int getMonths() {
            return months;
        }

        public SubPlan getPlan() {
            return plan;
        }
    }

    public static class Subgift extends UserNoticeEvent {
        private String planName;
        private int months;
        private SubPlan plan;
        private OfflineUser target;

        protected Subgift(SpannedString message, TagBag tags) {
            super(message, tags);
            type = MessageType.SUBGIFT;
            planName = tags.getOrDefault("msg-param-sub-plan-name", "??");
            plan = SubPlan.fromString(tags.get("msg-param-sub-plan"));
            months = Integer.parseInt(tags.getOrDefault("msg-param-months", "0"));
            String login = tags.getOrDefault("msg-param-recipient-user-name", "??");
            String display = tags.getOrDefault("msg-param-recipient-display-name", "??");
            int userid = Integer.parseInt(tags.getOrDefault("msg-param-recipient-id", "0"));
            target = new OfflineUser(login, display, userid);
        }

        public String getPlanName() {
            return planName;
        }

        public int getMonths() {
            return months;
        }

        public SubPlan getPlan() {
            return plan;
        }

        public OfflineUser getTarget() {
            return target;
        }
    }

    public static class Raid extends UserNoticeEvent {
        private int viewCount;
        private OfflineUser source;

        protected Raid(SpannedString message, TagBag tags) {
            super(message, tags);
            type = MessageType.RAID;
            viewCount = Integer.parseInt(tags.getOrDefault("msg-param-viewerCount", "0"));
            String login = tags.getOrDefault("msg-param-login", "??");
            String display = tags.getOrDefault("msg-param-displayName", "??");
            source = new OfflineUser(login, display, -1);
        }

        public int getViewCount() {
            return viewCount;
        }

        public OfflineUser getSource() {
            return source;
        }
    }

    public static class Ritual extends UserNoticeEvent {
        private String ritualName;

        protected Ritual(SpannedString message, TagBag tags) {
            super(message, tags);
            type = MessageType.RITUAL;
            ritualName = tags.getOrDefault("msg-param-ritual-name", "new_chatter");
        }

        /**
         * currently there's only the "new_chatter" ritual
         */
        public String getRitualName() {
            return ritualName;
        }
    }

    public static UserNoticeEvent getEventByTagBag(SpannedString message, TagBag tags) {
        switch (tags.get("msg-id").toLowerCase()) {
            case "sub": {
                return new Sub(message, tags);
            }
            case "resub": {
                return new Resub(message, tags);
            }
            case "subgift": {
                return new Subgift(message, tags);
            }
            case "raid": {
                return new Raid(message, tags);
            }
            case "ritual": {
                return new Ritual(message, tags);
            }
            default: {
                throw new IllegalStateException("Can't detect UserNoticeEvent-Type for msg-id " + tags.get("msg-id"));
            }
        }
    }
}
