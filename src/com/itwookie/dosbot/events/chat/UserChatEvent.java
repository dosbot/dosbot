package com.itwookie.dosbot.events.chat;

import com.itwookie.dosbot.events.Event;
import com.itwookie.dosbot.spanned.SpannedString;
import com.itwookie.dosbot.twitch.User;
import org.jetbrains.annotations.NotNull;

public class UserChatEvent implements Event {

    private User sender;
    private SpannedString message;

    public UserChatEvent(@NotNull User client, @NotNull SpannedString message) {
        this.sender = client;
        this.message = message;
    }

    public User getSender() {
        return sender;
    }

    public SpannedString getMessage() {
        return message;
    }
}
