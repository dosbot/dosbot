package com.itwookie.dosbot.events.chat;

import com.itwookie.dosbot.events.Event;
import com.itwookie.dosbot.twitch.User;
import org.jetbrains.annotations.NotNull;

public class UserConnectionEvent implements Event {

    protected User user;

    private UserConnectionEvent(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public static class Join extends UserConnectionEvent {
        public Join(@NotNull User user) {
            super(user);
        }
    }

    public static class Part extends UserConnectionEvent {
        public Part(@NotNull User user) {
            super(user);
        }
    }

}
