package com.itwookie.dosbot.events;

public abstract class CancelableEvent implements Event {

    private boolean isCancelled = false;

    public void setCancelled(boolean newState) {
        isCancelled = newState;
    }

    public boolean isCancelled() {
        return isCancelled;
    }

}
