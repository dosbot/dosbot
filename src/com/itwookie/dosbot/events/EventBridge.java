package com.itwookie.dosbot.events;

import com.itwookie.dosbot.TwitchClient;
import com.itwookie.dosbot.eventBus.EventBus;
import com.itwookie.dosbot.events.chat.UserChatEvent;
import com.itwookie.dosbot.events.chat.UserCommandEvent;
import com.itwookie.dosbot.events.chat.UserNoticeEvent;
import com.itwookie.dosbot.irc.CommandConsumer;
import com.itwookie.dosbot.irc.command.AbstractCommand;
import com.itwookie.dosbot.irc.command.icPrivmsg;
import com.itwookie.dosbot.irc.command.icTwitchUsernotice;
import com.itwookie.dosbot.spanned.SpannedString;

public class EventBridge implements CommandConsumer {

    private EventBus bus;
    private TwitchClient linkBack;

    public EventBridge(TwitchClient twitch) {
        this.bus = TwitchClient.getEventBus();
        this.linkBack = twitch;
    }

    @Override
    public void accept(AbstractCommand msg) {
        if (msg instanceof icPrivmsg) {
            icPrivmsg privmsg = (icPrivmsg) msg;
            if (privmsg.getMessage().matches("^![^\\s].*")) {
                createCommandEvent(privmsg);
            } else {
                createChatEvent(privmsg);
            }
        } else if (msg instanceof icTwitchUsernotice) {
            icTwitchUsernotice note = (icTwitchUsernotice) msg;
            createNoticeEvent(note);
        } else {
            int a = 1;
        }
    }

    private void createChatEvent(icPrivmsg privmsg) {
        SpannedString message = new SpannedString(privmsg.getMessage(), privmsg.getTags().getOrDefault("emotes", ""));
        bus.fire(new UserChatEvent(TwitchClient.getChannel().getUser(privmsg.getUser()).orElse(null), message));
    }

    private void createCommandEvent(icPrivmsg privmsg) {
        UserCommandEvent event = new UserCommandEvent(TwitchClient.getChannel().getUser(privmsg.getUser()).orElse(null), privmsg.getMessage());
        if (bus.fire(event)) { //if the command event was not cancelled
            TwitchClient.getCommands() //call the actual command handler
                    .getCommand(event.getCommandName())
                    .ifPresent(c ->
                            c.parseArgs(linkBack, event)
                    );
        }
    }

    private void createNoticeEvent(icTwitchUsernotice notice) {
        SpannedString message = new SpannedString(notice.getMessage(), notice.getTags().getOrDefault("emotes", ""));
        UserNoticeEvent event = UserNoticeEvent.getEventByTagBag(message, notice.getTags());
        bus.fire(event);
    }
}
