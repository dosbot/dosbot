package com.itwookie.dosbot.events.client;

import com.itwookie.dosbot.events.CancelableEvent;

public class TerminalCommandEvent extends CancelableEvent {

    private String command;
    private String arumentstring;

    public TerminalCommandEvent(String message) {
        if (message.indexOf(' ') > 1) {
            this.command = message.substring(0, message.indexOf(' '));
            this.arumentstring = message.substring(this.command.length() + 1);
        } else {
            this.command = message;
            this.arumentstring = "";
        }
    }

    /**
     * get the name for the chat command
     */
    public String getCommandName() {
        return command;
    }

    /**
     * returns the joined argument string for this chat command
     */
    public String getArumentString() {
        return arumentstring;
    }
}
