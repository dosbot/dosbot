package com.itwookie.dosbot.events.client;

import com.itwookie.dosbot.events.Event;

/**
 * This Event is called right after the client connected to the assigned channel.
 * You can use this event to retrieve the channel this bot just connected to.
 * The channel won't change during runtime.
 * The rest of initialisation (beside PreInitEvent) should probably be done here.
 */
public class PostConnectEvent implements Event {
    private String channel;

    public PostConnectEvent(String channel) {
    }

    public String getChannel() {
        return channel;
    }
}
