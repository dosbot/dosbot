package com.itwookie.dosbot.events.client;

import com.itwookie.dosbot.events.Event;

/**
 * This event is fired after the client finished oauth and ready to connect to the IRC server.
 * The client did not connect to the IRC server yet thus the channel and related APIs mit not work!
 *
 * @deprecated this event was removed for clarity and since there's no real difference to the {@link PreConnectEvent}
 */
@Deprecated
public class PostInitEvent implements Event {

}
