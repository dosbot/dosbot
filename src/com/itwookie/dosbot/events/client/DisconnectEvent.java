package com.itwookie.dosbot.events.client;

import com.itwookie.dosbot.events.Event;

/**
 * This Event is issued just before the client Parts the channel and disconnects from IRC.
 * This event is supposed to let you clean up stuff if necessary
 */
public class DisconnectEvent implements Event {
}
