package com.itwookie.dosbot.events.client;

import com.itwookie.dosbot.events.Event;

/**
 * This event is in a position similar to {@link PostInitEvent}
 */
public class PreConnectEvent implements Event {
}
