package com.itwookie.dosbot.events.client;

import com.itwookie.dosbot.events.Event;

/**
 * This event is called just before oauth is requested for the client.
 * You can use this event to prepare resources that are not critically depending on authorization.
 */
public class PreInitEvent implements Event {

}
