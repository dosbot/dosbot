package com.itwookie.dosbot.telnet;

import java.io.*;
import java.net.SocketException;
import java.net.SocketTimeoutException;

public class TelnetReceiver extends Thread implements Closeable {

    private boolean running = true;
    private BufferedReader in;

    private QueueHandler<String> rq = null;

    public TelnetReceiver(InputStream inputStream, QueueHandler<String> queueHandler) {
        setName("Telnet Receiver");
        in = new BufferedReader(new InputStreamReader(inputStream));
        rq = queueHandler;
    }

    @Override
    public void run() {
        while (running) {
            try {
                String message = in.readLine();
                if (message == null)
                    close();
                else {
//                    System.out.println("< "+message);
                    rq.enqueueMessage(message);
                }
            } catch (SocketTimeoutException ignore) {
                //continue
            } catch (SocketException socketClosed) {
                running = false;
                try {
                    in.close();
                } catch (Exception ignore) {
                }
//                socketClosed.printStackTrace();
            } catch (IOException exception) {
//                exception.printStackTrace();
            }
        }
    }

    @Override
    public void close() {
        try {
            in.close();
        } catch (Exception ignore) {
        }
        running = false;
    }

    void wait(int ms) {
        try {
            Thread.sleep(ms);
        } catch (Exception ignore) {
        }
    }
}
