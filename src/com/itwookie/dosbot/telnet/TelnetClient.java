package com.itwookie.dosbot.telnet;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class TelnetClient implements AutoCloseable {

    private InetSocketAddress address;
    private Socket telnet;

    public TelnetClient(InetSocketAddress address) {
        this.address = address;
        telnet = new Socket();
    }

    public TelnetClient(InetAddress address, int port) throws IOException {
        this(new InetSocketAddress(address, port));
    }

    protected TelnetReceiver rx = null;
    protected QueueHandler rq = new QueueHandler(true);
    protected TelnetTransmitter tx = null;
    protected QueueHandler tq = new QueueHandler(false);

    /** */
    public void connect(int timeout) throws IOException {
        telnet.connect(address, timeout);
        telnet.setSoTimeout(100);
        telnet.setKeepAlive(true);
        telnet.setReuseAddress(true);
        rx = new TelnetReceiver(telnet.getInputStream(), rq);
        tx = new TelnetTransmitter(telnet.getOutputStream(), tq, 20, 30000);
        rx.start();
        rq.start();
        tx.start();
        tq.start();
    }

    public void connect() throws IOException {
        connect(1500);
    }

    public boolean isConnected() {
        return telnet.isConnected() && !telnet.isClosed() && !telnet.isInputShutdown() && !telnet.isOutputShutdown();
    }

    public void send(String message) throws IllegalStateException {
        if (tx == null) throw new IllegalStateException("Connection not established");
        tq.enqueueMessage(message);
    }

    /**
     * set the new messages over milliseconds limit for the transmitter
     */
    public void changeRate(int messages, int milliseconds) {
        if (tx == null) throw new IllegalStateException("Connection not established");
        tx.setMessageLimit(messages, milliseconds);
    }

    public void addMessageListener(Consumer<String> listener) {
        rq.addMessageHandler(listener);
    }

    public void removeMessageListener(Consumer<String> listener) {
        rq.removeMessageHander(listener);
    }

    public void addMessageFilter(Predicate<String> filter) {
        rq.addMessageFilter(filter);
    }

    public void removeMessageFilter(Predicate<String> filter) {
        rq.removeMessageFilter(filter);
    }

    /**
     * @return true if both, send and receive queues are empty
     */
    public boolean emptyQueues() {
        return rq.queueSize() == 0 && tq.queueSize() == 0;
    }

    @Override
    public void close() {
        rx.close();
        rq.close();
        tx.close();
        tq.close();
        try {
            telnet.close();
        } catch (Exception e) {
            /**/
        }
    }

}
