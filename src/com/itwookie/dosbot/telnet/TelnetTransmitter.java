package com.itwookie.dosbot.telnet;

import java.io.*;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Optional;

public class TelnetTransmitter extends Thread implements Closeable {

    private boolean running = true;

    private BufferedWriter out;
    private QueueHandler tq;

    private int maxSize, maxAge;
    private ArrayList<Long> messageTimestamps = new ArrayList<>(30);

    /**
     * with limit and timeout you can specify how many messages can be sent in a certain time-span.
     *
     * @param limit   is the amount of messages that can be sent within a time-span
     * @param timeout is the time in milliseconds for a message to count against the limit
     */
    public TelnetTransmitter(OutputStream outputStream, QueueHandler queueHandler, int limit, int timeout) {
        setName("Telnet Transmitter");
        out = new BufferedWriter(new OutputStreamWriter(outputStream));
        tq = queueHandler;
        maxSize = limit;
        maxAge = timeout;
    }

    public TelnetTransmitter(OutputStream outputStream, QueueHandler queueHandler) {
        this(outputStream, queueHandler, Integer.MAX_VALUE, 0);
    }

    public void setMessageLimit(int limit, int timeout) {
        maxSize = limit;
        maxAge = timeout;
    }

    @Override
    public void run() {
        while (running) {
            messageTimestamps.removeIf((timestamp) -> System.currentTimeMillis() - timestamp >= maxAge);
            if (messageTimestamps.size() < maxSize) {
                try {
                    Optional<String> msg = tq.dequeueMessage();
                    if (msg.isPresent()) {
//                        System.out.println("> "+msg.get());
                        out.write(msg.get());
                        out.write("\r\n");
                        out.flush();
                        messageTimestamps.add(System.currentTimeMillis());
                    } else {
                        wait(100);
                    }
                } catch (SocketException socketClosed) {
                    running = false;
                    try {
                        out.close();
                    } catch (Exception ignore) {
                    }
//                    socketClosed.printStackTrace();
                } catch (IOException exception) {
//                    exception.printStackTrace();
                }
            } else {
                wait(100);
            }
            Thread.yield();
        }
    }

    @Override
    public void close() {
        try {
            out.close();
        } catch (Exception ignore) {
        }
        running = false;
    }

    void wait(int ms) {
        try {
            Thread.sleep(ms);
        } catch (Exception ignore) {
        }
    }
}
