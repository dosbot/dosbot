package com.itwookie.dosbot.telnet;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class QueueHandler<T> extends Thread implements Closeable {

    private boolean running = true;
    private LinkedList<T> messages = new LinkedList<>();
    private List<Consumer<T>> handler = new ArrayList<>();
    private List<Predicate<T>> filter = new ArrayList<>();
    private final Object messageMutex = new Object();

    private boolean autoDequeue;

    /**
     * @param autoDequeue if true messages will automatically be de-queued by the thread and passed to the registered handlers.
     *                    otherwise the thread won't start and you have to manually de-queue messages with dequeueMessage()
     */
    public QueueHandler(boolean autoDequeue) {
        setName("Queue Handler");
        this.autoDequeue = autoDequeue;
    }

    public void addMessageHandler(Consumer<T> handler) {
        synchronized (messageMutex) {
            this.handler.add(handler);
        }
    }

    public void removeMessageHander(Consumer<T> handler) {
        synchronized (messageMutex) {
            this.handler.remove(handler);
        }
    }

    /**
     * filters are able to reduce the impact on handler by removing junk-messages
     */
    public void addMessageFilter(Predicate<T> filter) {
        synchronized (messageMutex) {
            this.filter.add(filter);
        }
    }

    public void removeMessageFilter(Predicate<T> filter) {
        synchronized (messageMutex) {
            this.filter.remove(filter);
        }
    }

    public void enqueueMessage(T message) {
        synchronized (messageMutex) {
            messages.add(message);
        }
    }

    /**
     * @return the next message or empty if no more messages are queued
     * @throws IllegalStateException if this queue is in autoDequeue mode
     */
    public Optional<T> dequeueMessage() {
        if (autoDequeue) throw new IllegalStateException("Thread in auto-dequeue mode!");
        synchronized (messageMutex) {
            if (messages.isEmpty())
                return Optional.empty();
            else
                return Optional.of(messages.pop());
        }
    }

    @Override
    public void run() {
        T message;
        while (running && autoDequeue) {
            synchronized (messageMutex) {
                if (messages.isEmpty())
                    message = null;
                else
                    message = messages.pop();
            }
            if (message != null) {
                synchronized (messageMutex) {
                    final T msg = message;
                    boolean goOn = true;
                    for (Predicate<T> f : filter) {
                        if (!f.test(msg))
                            goOn = false;
                    }
                    if (goOn)
                        for (Consumer<T> consumer : handler)
                            consumer.accept(msg);
                }
                Thread.yield();
            } else {
                wait(100);
            }
        }
    }

    @Override
    public void close() {
        running = false;
    }

    public int queueSize() {
        synchronized (messageMutex) {
            return messages.size();
        }
    }

    void wait(int ms) {
        try {
            Thread.sleep(ms);
        } catch (Exception ignore) {
        }
    }
}
