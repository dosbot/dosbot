package com.itwookie.dosbot;

import com.itwookie.dosbot.commands.*;
import com.itwookie.dosbot.eventBus.Subscribe;
import com.itwookie.dosbot.events.chat.UserChatEvent;
import com.itwookie.dosbot.twitch.Channel;
import com.itwookie.dosbot.twitch.Terminal;
import com.itwookie.dosbot.twitch.User;
import com.itwookie.logger.Logger;
import com.itwookie.utils.Timespan;

import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

/**
 * internal core "plugin" for the client providing basic functionality
 */
public class ClientCore {

    private static Logger log = Logger.getLogger();
    private static Logger chat = Logger.getLogger("Chat");
    private static CommandExecutor sayCmd = (caller, args) -> {
        if (caller instanceof Terminal) {
            TwitchClient.sendMessage(args.<String>get("message").get());
        }
    };


    @Subscribe
    public static void onUserChat(UserChatEvent event) {
        chat.log(event.getSender().getDisplayName() + ": " + event.getMessage());
    }

    private static CommandExecutor followCmd = (caller, args) -> {
        if (caller instanceof User) {
            User target = args.<User>get("user").orElse((User) caller);
            Optional<Date> follows = target.getFollowDate();
            if (follows.isPresent()) {
                Timespan time = new Timespan(follows.get(), System.currentTimeMillis());
                log.log(String.format("%s is following for %s", target.getDisplayName(), time.toString()));
            } else {
                log.log(String.format("%s is not following", target.getDisplayName()));
            }
        } else {
            Optional<User> target = args.get("user");
            if (!target.isPresent()) {
                log.log("Terminal has to specify a target");
            } else {
                Optional<Date> follows = target.get().getFollowDate();
                if (follows.isPresent()) {
                    Timespan time = new Timespan(follows.get(), System.currentTimeMillis());
                    log.log(String.format("%s is following for %s", target.get().getDisplayName(), time.toString()));
                } else {
                    log.log(String.format("%s is not following", target.get().getDisplayName()));
                }
            }
        }
    };
    private static CommandExecutor userNfoCmd = (caller, args) -> {
        Optional<User> target = args.get("user");
        if (!target.isPresent()) {
            log.log("Missing user");
        } else {
            User user = target.get();
            log.log("--- " + user.getDisplayName() + " ---",
                    "\nLogin: ", user.getUserName(),
                    "\nUserID: ", user.getUserID(),
                    "\nIs Mod: ", user.isMod(),
                    "\nBadges: ", Arrays.toString(user.getBadges().toArray(new String[0]))
            );
        }
    };
    private static CommandExecutor roomNfoCmd = (caller, args) -> {
        Channel room = TwitchClient.getChannel();
        log.log("--- #" + room.getName() + " ---",
                "\nRoomID: ", room.getRoomID(),
                "\nFollower-Only (months): ", room.getFollowerOnly().orElse(0),
                "\nSlow-Mode (seconds): ", room.getSlow(),
                "\nEmote-Only: ", room.isEmoteOnly(),
                "\nr9k: ", room.isR9k(),
                "\nSub-Only: ", room.isSubsOnly(),
                "\nLanguage: ", room.getBroadcasterLang().orElse(null)
        );
    };

    static void init() {
        TwitchClient.getCommands().register(Command.builder("say")
                .args(Arguments.builder()
                        .next(new RemainingStringArgument("message"))
                        .build())
                .execute(sayCmd)
                .build());

        TwitchClient.getCommands().register(Command.builder("follows")
                .args(Arguments.builder()
                        .next(new OptionalArgument<>(new UserArgument("user")))
                        .build())
                .execute(followCmd)
                .build());

        TwitchClient.getCommands().register(Command.builder("userinfo")
                .args(Arguments.builder()
                        .next(new UserArgument("user"))
                        .build())
                .execute(userNfoCmd)
                .build());

        TwitchClient.getCommands().register(Command.builder("roominfo")
                .args(Arguments.builder()
                        .build())
                .execute(roomNfoCmd)
                .build());

        TwitchClient.getEventBus().findSubscribersFor(ClientCore.class);
    }

}
