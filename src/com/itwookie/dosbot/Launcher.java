package com.itwookie.dosbot;

import com.itwookie.dosbot.commands.Command;
import com.itwookie.dosbot.commands.CommandExecutor;
import com.itwookie.dosbot.twitch.Terminal;
import com.itwookie.logger.Logger;
import com.itwookie.utils.Stoppable;

import javax.swing.*;

public class Launcher {

    private static boolean running = true;
    private static final Object statusMutex = new Object();
    private static Stoppable logWatcher = new Stoppable() {

        @Override
        public void onStart() {
            setName("LogWatcher");
            Logger.showMultiTabLogger(this::halt);
        }

        @Override
        public void onLoop() {
            try {
                Thread.sleep(100);
            } catch (InterruptedException ignore) {
                /**/
            }
        }

        @Override
        public void onHalt() {
            synchronized (statusMutex) {
                Launcher.running = false;
            }
            Logger.close();
        }
    };

    static Logger log = Logger.getLogger();

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
        } catch (Exception eh) {
            /**/
        }

        logWatcher.start();

        TwitchClient twitch = null;
        try {
            twitch = new TwitchClient();
            TwitchClient.getCommands().register(Command.builder("exit")
                    .execute(exitCmd)
                    .build());
            ClientCore.init();

            new TerminalInput(twitch).start();

            boolean localRunning;
            while (TwitchClient.isConnected()) {
                synchronized (statusMutex) {
                    localRunning = running;
                }
                if (!localRunning)
                    twitch.disconnect();

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    /**/
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (twitch != null)
                twitch.disconnect();
        }
    }

    private static CommandExecutor exitCmd = (caller, args) -> {
        if (caller instanceof Terminal) {
            synchronized (statusMutex) {
                running = false;
            }
        }
    };
}
