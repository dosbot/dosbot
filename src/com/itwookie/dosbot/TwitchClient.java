package com.itwookie.dosbot;

import com.itwookie.dosbot.api.EndpointManager;
import com.itwookie.dosbot.api.Endpoints;
import com.itwookie.dosbot.commands.CommandRegistry;
import com.itwookie.dosbot.eventBus.EventBus;
import com.itwookie.dosbot.events.EventBridge;
import com.itwookie.dosbot.events.client.DisconnectEvent;
import com.itwookie.dosbot.events.client.PostConnectEvent;
import com.itwookie.dosbot.events.client.PreConnectEvent;
import com.itwookie.dosbot.events.client.PreInitEvent;
import com.itwookie.dosbot.exceptions.ConnectException;
import com.itwookie.dosbot.exceptions.UnexpectedResponseException;
import com.itwookie.dosbot.irc.IRCClient;
import com.itwookie.dosbot.irc.TagBag;
import com.itwookie.dosbot.irc.command.*;
import com.itwookie.dosbot.oauth2.OAuth2;
import com.itwookie.dosbot.oauth2.OAuthConfiguration;
import com.itwookie.dosbot.oauth2.Request;
import com.itwookie.dosbot.twitch.Channel;
import com.itwookie.dosbot.twitch.Terminal;
import com.itwookie.dosbot.twitch.User;
import com.itwookie.inireader.INIConfig;
import com.itwookie.logger.Logger;
import com.itwookie.pluginloader.PluginContainer;
import com.itwookie.pluginloader.PluginLoader;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Optional;

public class TwitchClient {

    private INIConfig bot = new INIConfig();
    private IRCClient chat;
    private boolean hasConnected = false;
    private EventBus events = new EventBus();
    private String botAccount;
    private OAuth2 oauth2;
    private PluginLoader plugins;
    private CommandRegistry commandRegistry = new CommandRegistry();
    private Terminal sender = new Terminal();
    private static TwitchClient singleton = null;
    private EndpointManager twitchApi = null;
    private static Logger log = Logger.getLogger("twitch");

    public static Logger getLogger() {
        return log;
    }

    TwitchClient() throws IOException {
        if (singleton == null) {
            singleton = this;

            //region Load resources
            {
                INIConfig ini = new INIConfig();
                ini.loadFrom(ClassLoader.getSystemResourceAsStream("oauth.ini"));
                singleton.oauth2 = OAuthConfiguration.builder()
                        .auth(ini.get("twitch", "ai")/*, ini.get("twitch", "as")*/)
                        .scopes(ini.get("twitch", "scope"))
                        .redirectURL(ini.get("twitch", "redirect"))
                        .authURL(new URL("https://id.twitch.tv/oauth2/authorize"))
                        .tokenURL(new URL("https://id.twitch.tv/oauth2/token"))
                        .build();
            }

            try {
                singleton.plugins = new PluginLoader();
                Collection<PluginContainer> plcs = singleton.plugins.scanDirectory(new File("plugins"));
                singleton.plugins.loadPlugins(plcs, false, false);
                //log to default since plugins are not twitch domain
                Logger.getLogger().log("Loaded plugins:");
                for (PluginContainer plc : plcs) {
                    Logger.getLogger().yell(String.format("- %s %s (by %s):", plc.getName(), plc.getVersion(), plc.getAuthor()));
                    Logger.getLogger().log(plc.getDescription());
                    singleton.events.findSubscribersFor(plc.getPlugin());
                }
            } catch (Exception e) {
                singleton.plugins.destruct();
                singleton.events.unsubscribeAll();
                //twitch client itself subscribes after this constructor, so basic functionality should still be present
                e.printStackTrace();
            }

            singleton.bot.loadFrom(new File("config.ini"));
            //endregion

//            events.findSubscribersFor(Launcher.class);
            init();

            //region logging
            //this block is to print commands i'm not yet handling
            chat.addMessageListener(
                    p -> {
                        if (p instanceof icDummyUnknown) {
                            log.warn(p.toString());
                        } else if (!(p instanceof icPrivmsg)) {
                            log.log(p.toString());
                        }
                    }
            );
            //endregion

            connect();
        } else throw new IllegalStateException("The Twitch Client is already running");
    }

    public static EndpointManager getTwitchAPI() {
        return singleton.twitchApi;
    }

    /**
     * @throws UnknownHostException if twitch decides to move the irc servers
     * @throws IOException          if communication fails
     */
    private void init() throws IOException {
        singleton.events.start();

        singleton.events.fire(new PreInitEvent());
//        singleton.botAccount = singleton.bot.get("chat", "account").toLowerCase();

        //important for chat login and further requests
        singleton.oauth2.auth();
        if (singleton.oauth2.getCurrentAccessToken() == null)
            throw new ConnectException("OAuth might have been declined");
        //start somewhat related enpoint manager
        twitchApi = new EndpointManager("https://api.twitch.tv/helix/", oauth2);
        twitchApi.start();

        //prepare irc client
        singleton.chat = new IRCClient(InetAddress.getByName("irc.chat.twitch.tv"), 6667);
        singleton.chat.useChannel(singleton.bot.get("chat", "channel"));

        //prepare by setting self (bot account)
        getSelfFromToken();

        //implement keepalive
        singleton.chat.addMessageFilter(t -> {
            if (t instanceof icPing) {
                singleton.chat.send(new icPong(((icPing) t).getServer()));
                return false;
            } else return !(t instanceof icPong);
        });

        //link events
        singleton.chat.addMessageListener(new EventBridge(singleton));
//        singleton.events.fire(new PostInitEvent());
    }

    private void getSelfFromToken() {
        try {
            Request request = twitchApi.build(Endpoints.USERS_NO_MAIL);
            JSONObject obj = twitchApi.send(request).get();
            JSONArray data = obj.getJSONArray("data");
            if (data.length() != 1)
                throw new IllegalStateException("Could not retrieve login");
            obj = data.getJSONObject(0);
            TagBag tags = new TagBag();
            tags.put("user-id", obj.getString("id"));
            tags.put("display-name", obj.getString("display_name"));
            botAccount = obj.getString("login");
            User user = new User(botAccount, tags);
            chat.getChannel().__innternalAddUser(user, false);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @throws ConnectException if a error occurred while establishing a connection
     * @throws IOException      for other, generic network errors
     */
    private void connect() throws IOException {
        if (singleton.hasConnected) throw new IllegalStateException("Already connected");
        singleton.hasConnected = true;
        singleton.events.fire(new PreConnectEvent());

        //connect to server
        try {
            singleton.chat.getTelnet().connect(1000);
        } catch (IOException e) {
            throw new ConnectException("Could not connect to irc", e);
        }

        //auth and join channel
        singleton.chat
                .send(new icCap("twitch.tv/tags"))
                .await(AbstractCommand.class, null, p -> {
                    if (!(p instanceof icCap))
                        throw new UnexpectedResponseException("Capability rejected: 'twitch.tv/tags'", p.getMessageRaw().get());
                    else
                        log.log("Capability 'twitch.tv/tags' accepted");
                })
                .send(new icCap("twitch.tv/membership"))
//            .await(icCap.class, null, null)
                .await(AbstractCommand.class, null, p -> {
                    if (!(p instanceof icCap))
                        throw new UnexpectedResponseException("Capability rejected: 'twitch.tv/membership'", p.getMessageRaw().get());
                    else
                        log.log("Capability 'twitch.tv/membership' accepted");
                })
                .send(new icCap("twitch.tv/commands"))
                .await(icCap.class, null, p -> {
                    if (!(p instanceof icCap))
                        throw new UnexpectedResponseException("Capability rejected: 'twitch.tv/commands'", p.getMessageRaw().get());
                    else
                        log.log("Capability 'twitch.tv/commands' accepted");
                })
                .send(new icPass("oauth:" + singleton.oauth2.getCurrentAccessToken()))
                .send(new icNick(singleton.botAccount))
                .await(icReply.class, p -> ((icReply) p).getReply().equalsIgnoreCase("rpl_endofmotd"), null)
                .send(new icJoin(singleton.chat.getChannel().getName()))
                .await(icJoin.class, null, null);
        singleton.events.fire(new PostConnectEvent(singleton.chat.getChannel().getName()));
    }

    void disconnect() {
        if (!singleton.hasConnected || !singleton.chat.getTelnet().isConnected()) return;
        singleton.events.fire(new DisconnectEvent());

        //disconnect
        singleton.chat
                .send(new icPart(singleton.chat.getChannel().getName())).await(100)
                .await(icPart.class, p -> ((icPart) p).getUser().equalsIgnoreCase(botAccount), null)
                .send(new icQuit("Good bye Kraken"))
                .await(100)
                .close();

        singleton.events.close();
        singleton.twitchApi.close();
        singleton.plugins.destruct();
    }

    /**
     * send a message to twitch chat
     */
    public static void sendMessage(String message) {
        singleton.chat.send(new icPrivmsg("#" + singleton.chat.getChannel().getName(), message));
    }

    static boolean isConnected() {
        return singleton.chat.getTelnet().isConnected();
    }

//    public static IRCClient getIRCClient() {
//        return singleton.chat;
//    }

    public static EventBus getEventBus() {
        return singleton.events;
    }

    public static Channel getChannel() {
        return singleton.chat.getChannel();
    }

    public static CommandRegistry getCommands() {
        return singleton.commandRegistry;
    }

    /**
     * will always work but may have very limited information at the very beginning and after parting
     *
     * @return the User representing the bot
     */
    public static User getSelf() {
        Optional<User> myself = getChannel().getUser(singleton.botAccount);
        return myself.orElseGet(() -> {
            User newUser = new User(singleton.botAccount, new TagBag());
            getChannel().__innternalAddUser(newUser, false);
            return newUser;
        });
    }

    public static Collection<PluginContainer> getPluginsByName(String name) {
        return singleton.plugins.getPluginsByName(name);
    }

    public static Collection<PluginContainer> getPluginsByClass(Class<?> pluginClass) {
        return singleton.plugins.getPluginsBySuperclass(pluginClass);
    }

    /**
     * used to get information about the terminal sending commands onto the bus
     */
    public static Terminal getSender() {
        return singleton.sender;
    }

}
