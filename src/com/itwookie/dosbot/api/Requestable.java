package com.itwookie.dosbot.api;

import org.intellij.lang.annotations.MagicConstant;

public interface Requestable {
    String getMethod();

    String getUri();

    /**
     * @return true if all necessary data were fed to this object
     */
    boolean isReady();

    /**
     * If a endpoint requires a scope, this means that the bearer token is required.
     * otherwise the client-id is enough
     *
     * @return the scope if required or empty string otherwise
     */
    @MagicConstant(valuesFromClass = Scopes.class)
    String getScope();
}
