package com.itwookie.dosbot.api;

import java.util.*;

public class EndpointInstance implements Requestable {

    private String scope;
    private String uri;
    private String method;
    private Map<String, String> params = new HashMap<>();

    private EndpointInstance() {
    }

    @Override
    public String getUri() {
        return uri;
    }

    @Override
    public String getMethod() {
        return method;
    }

    @Override
    public boolean isReady() {
        return true;
    }

    @Override
    public String getScope() {
        return scope;
    }

    /**
     * changes in this map might be reflected into the underlying map, but won't affect the
     * target uri
     */
    Map<String, String> getParams() {
        return params;
    }

    public static class Builder {
        EndpointInstance instance = new EndpointInstance();
        Endpoint ep;

        private Builder(Endpoint endpoint) {
            ep = endpoint;
            instance.method = ep.getMethod();
            instance.scope = ep.getScope();
        }

        public Builder set(String parameter, String value) {
            parameter = parameter.toLowerCase();
            if (!ep.getParams().contains(parameter))
                throw new IllegalArgumentException("Unknown parameter for this endpoint");
            instance.params.put(parameter, value);
            return Builder.this;
        }

        public Builder set(String parameter, Integer value) {
            parameter = parameter.toLowerCase();
            if (!ep.getParams().contains(parameter))
                throw new IllegalArgumentException("Unknown parameter for this endpoint");
            instance.params.put(parameter, value.toString());
            return Builder.this;
        }

        public EndpointInstance build() {
            Set<String> check = new HashSet<>(ep.getParams());
            check.removeAll(instance.params.keySet());
            if (!check.isEmpty())
                throw new IllegalStateException("Missing Parameters: " + Arrays.toString(check.toArray()));
            String[] parts = ep.getParams().toArray(new String[0]);
            for (int i = 0; i < parts.length; i++) {
                String pname = parts[i].substring(1).toLowerCase();
                if (instance.params.containsKey(pname))
                    parts[i] = instance.params.get(pname);
            }
            String stiched = String.join("/", parts);
            instance.uri = "/" + stiched;
            return instance;
        }
    }

    static EndpointInstance.Builder builder(Endpoint ep) {
        return new Builder(ep);
    }

}
