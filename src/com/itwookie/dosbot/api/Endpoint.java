package com.itwookie.dosbot.api;

import org.intellij.lang.annotations.MagicConstant;

import java.util.HashSet;
import java.util.Set;

public class Endpoint implements Requestable {

    private String scope;
    private String uri;
    private String method;
    private Set<String> params = new HashSet<>();

    @Override
    public String getUri() {
        return uri;
    }

    @Override
    public String getMethod() {
        return method;
    }

    public Set<String> getParams() {
        return params;
    }

    @Override
    public boolean isReady() {
        return params.size() == 0;
    }

    @Override
    public String getScope() {
        return scope;
    }

    private Endpoint() {
    }

    /**
     * enpoint definition where :&lt;NAME&gt; parts are interpreted as parameters.
     * all parameters have to be set in a EndpointInstance before it can return a request.
     */
    Endpoint(@MagicConstant(valuesFromClass = Scopes.class) String scope, @MagicConstant(stringValues = {"GET", "PUT", "POST", "DELETE", "HEAD", "OPTIONS", "TRACE"}) String method, String definition) {
        if (definition.charAt(0) == '/')
            definition = definition.substring(1);
        this.uri = definition;
        this.method = method;
        this.scope = scope;
        for (String part : definition.split("/")) {
            if (part.charAt(0) == ':') {
                params.add(part.substring(1));
            }
        }
    }

    public EndpointInstance.Builder builder() {
        return EndpointInstance.builder(this);
    }

}
