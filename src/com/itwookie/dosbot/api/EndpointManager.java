package com.itwookie.dosbot.api;

import com.itwookie.dosbot.oauth2.OAuth2;
import com.itwookie.dosbot.oauth2.Request;
import com.itwookie.logger.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class EndpointManager extends Thread implements Closeable {

    private boolean running = true;
    private String baseURL;
    private final Object queueMutex = new Object();
    private List<Request> requestsQueue = new LinkedList<>();
    private List<CompletableFuture<JSONObject>> responseQueue = new LinkedList<>();

    private int Ratelimit_Remaining = 30; //start with unauthorized count, will fix itself after first request
    private long Ratelimit_Reset = System.currentTimeMillis();

    private OAuth2 authenticator;

    public EndpointManager(String baseURL, OAuth2 authenticator) throws MalformedURLException {
        setName("Endpoint Manager");
        this.baseURL = baseURL;
        this.authenticator = authenticator;
        if (!this.baseURL.endsWith("/")) {
            this.baseURL = this.baseURL + "/";
        }
        new URL(this.baseURL);
    }

    /**
     * Parametrize an endpoint if necessary and pass it here.<br>
     * The returned request can be used to set further data like query arguments or post data.<br>
     * One the request is ready you can enqueue it with send(Request)
     *
     * @param endpoint a parametrized EndpointInstance
     * @return a request that may be further fed with further data
     */
    public Request build(Requestable endpoint) throws MalformedURLException {
        if (!endpoint.isReady())
            throw new IllegalStateException("The supplied endpoint required a EndpointInstance. Call .build() on the Endpoint");
        if (!endpoint.getScope().isEmpty() && !authenticator.hasScope(endpoint.getScope()))
            throw new IllegalStateException("The required scope for " + endpoint.getUri() + " (" + endpoint.getScope() + ") is currently not granted!");

        return new Request(endpoint.getMethod(), this.baseURL + endpoint.getUri());
    }

    /**
     * Enqueues the request and sends it as soon as the rate limit allows it to be sent.
     * You can poll for the future to be completed or block with get().
     */
    public CompletableFuture<JSONObject> send(Request request) {
        CompletableFuture<JSONObject> result = new CompletableFuture<>();
        synchronized (queueMutex) {
            requestsQueue.add(request);
            responseQueue.add(result);
        }
        return result;
    }

    /**
     * internal send method - extracted for readability.<br>
     * make sure to run in queueMutex context
     *
     * @return true if sending was possible with ratelimit
     */
    private boolean _send() {
        if (Ratelimit_Remaining <= 0) return false;

        //create connection
        HttpsURLConnection connection = null;
        try {
            Request r = requestsQueue.get(0);
            Logger.getLogger("twitch-API").log("Sending:\n", r);
            connection = (HttpsURLConnection) authenticator.request(r, (con) -> {
            });

            if (connection.getResponseCode() > 299 || connection.getResponseCode() < 200)
                return false;
        } catch (IOException e) {
            if (connection != null)
                connection.disconnect();
            return false;
        }

        //prepare string reader
        StringBuilder sb = null;
        try {
            int len = Integer.valueOf(connection.getHeaderField("content-length"));
            sb = new StringBuilder(len);
        } catch (NumberFormatException e) {
            connection.disconnect();
            e.printStackTrace();
            return false;
        }

        //read response
        InputStreamReader isr = null;
        try {
            isr = new InputStreamReader(connection.getInputStream());

            char[] buffer = new char[256];
            int r;
            while ((r = isr.read(buffer, 0, buffer.length)) > 0) {
                sb.append(buffer, 0, r);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (isr != null)
                try {
                    isr.close();
                } catch (Exception e) {/**/}
        }

        //update bucket
        try {
            Ratelimit_Remaining = Integer.valueOf(connection.getHeaderField("Ratelimit-Remaining"));
            Ratelimit_Reset = Long.valueOf(connection.getHeaderField("Ratelimit-Reset")) * 1000;

            Logger.getLogger("twitch-API").log("Tickets remaining: ", Ratelimit_Remaining, ", Reset in: ", Math.max(0, (Ratelimit_Reset - System.currentTimeMillis()) / 1000));
        } catch (NumberFormatException e) {
            /**/
        }

        connection.disconnect();

        //construct json object
        try {
            JSONObject object = new JSONObject(sb.toString());
            //pretty print
            Logger.getLogger("twitch-API").log("Response:\n", object.toString(2));

            requestsQueue.remove(0);
            responseQueue.remove(0).complete(object);
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public void run() {
        long sleep = 100;
        while (running) {
            synchronized (queueMutex) {
                if (responseQueue.size() > 0) {
                    sleep = !_send() ? Math.max(100, Ratelimit_Reset - System.currentTimeMillis()) : 10;
                } else sleep = Math.max(100, Ratelimit_Reset - System.currentTimeMillis());
            }
            if (sleep > 0) s(sleep);
        }
    }

    @Override
    public void close() {
        running = false;
    }

    private void s(long ms) {
        try {
            Thread.sleep(ms);
        } catch (Exception e) {/**/}
    }
}
