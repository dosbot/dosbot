package com.itwookie.dosbot.api;

public class Endpoints {

    /**
     * Gets a URL that extension developers can use to download analytics reports (CSV files) for their extensions. The URL is valid for 5 minutes. For detail about analytics and the fields returned, see the Insights & Analytics guide.
     *
     * @apiNote <a href="https://dev.twitch.tv/docs/api/reference/#get-extension-analytics">https://dev.twitch.tv/docs/api/reference/#get-extension-analytics</a>
     */
    public static final Endpoint ANALYTICS_EXTENSIONS = new Endpoint(Scopes.NEW_ANALYTICS_READ_EXTENSIONS, "GET", "/analytics/extensions");

    /**
     * Gets a URL that game developers can use to download analytics reports (CSV files) for their games. The URL is valid for 5 minutes. For detail about analytics and the fields returned, see the Insights & Analytics guide.<br>
     * The response has a JSON payload with a data field containing an array of games information elements and can contain a pagination field containing information required to query for more streams.
     *
     * @apiNote <a href="https://dev.twitch.tv/docs/api/reference/#get-game-analytics">https://dev.twitch.tv/docs/api/reference/#get-game-analytics</a>
     */
    public static final Endpoint ANALYTICS_GAMES = new Endpoint(Scopes.NEW_ANALYTICS_READ_GAMES, "GET", "/analytics/games");

    /**
     * Gets a ranked list of Bits leaderboard information for an authorized broadcaster.
     *
     * @apiNote <a href="https://dev.twitch.tv/docs/api/reference/#get-bits-leaderboard">https://dev.twitch.tv/docs/api/reference/#get-bits-leaderboard</a>
     */
    public static final Endpoint BITS_LEADERBOARD = new Endpoint(Scopes.NEW_BITS_READ, "GET", "/bits/leaderboard");

    /**
     * Creates a clip programmatically. This returns both an ID and an edit URL for the new clip.<br>
     * Clip creation takes time. We recommend that you query Get Clips, with the clip ID that is returned here. If Get Clips returns a valid clip, your clip creation was successful. If, after 15 seconds, you still have not gotten back a valid clip from Get Clips, assume that the clip was not created and retry Create Clip.<br>
     * This endpoint has a global rate limit, across all callers. The limit may change over time, but the response includes informative headers:
     * <pre>
     * Ratelimit-Helixclipscreation-Limit: &lt;int value&gt;
     * Ratelimit-Helixclipscreation-Remaining: &lt;int value&gt;
     * </pre>
     *
     * @apiNote <a href="https://dev.twitch.tv/docs/api/reference/#create-clip">https://dev.twitch.tv/docs/api/reference/#create-clip</a>
     */
    public static final Endpoint CLIPS_CREATE = new Endpoint(Scopes.NEW_CLIPS_EDIT, "POST", "/clips");

    /**
     * Gets clip information by clip ID (one or more), broadcaster ID (one only), or game ID (one only).<br>
     * The response has a JSON payload with a data field containing an array of clip information elements and a pagination field containing information required to query for more streams.
     *
     * @apiNote <a href="https://dev.twitch.tv/docs/api/reference/#get-clips">https://dev.twitch.tv/docs/api/reference/#get-clips</a>
     */
    public static final Endpoint CLIPS_GET = new Endpoint(Scopes.NONE, "GET", "/clips");

    /**
     * Creates a URL where you can upload a manifest file and notify users that they have an entitlement. Entitlements are digital items that users are entitled to use. Twitch entitlements are granted to users gratis or as part of a purchase on Twitch.<br>
     * See the Drops Guide for details about using this endpoint to notify users about Drops.
     *
     * @apiNote <a href="https://dev.twitch.tv/docs/api/reference/#create-entitlement-grants-upload-url">https://dev.twitch.tv/docs/api/reference/#create-entitlement-grants-upload-url</a>
     */
    public static final Endpoint ENTITLEMENTS_UPLOAD = new Endpoint(Scopes.NONE, "POST", "/entitlements/upload");

    /**
     * Gets game information by game ID or name.<br>
     * The response has a JSON payload with a data field containing an array of games elements.
     *
     * @apiNote <a href="https://dev.twitch.tv/docs/api/reference/#get-games">https://dev.twitch.tv/docs/api/reference/#get-games</a>
     */
    public static final Endpoint GAMES = new Endpoint(Scopes.NONE, "GET", "/games");

    /**
     * Gets games sorted by number of current viewers on Twitch, most popular first.<br>
     * The response has a JSON payload with a data field containing an array of games information elements and a pagination field containing information required to query for more streams.
     *
     * @apiNote <a href="https://dev.twitch.tv/docs/api/reference/#get-top-games">https://dev.twitch.tv/docs/api/reference/#get-top-games</a>
     */
    public static final Endpoint GAMES_TOP = new Endpoint(Scopes.NONE, "GET", "/games/top");

    /**
     * Gets information about active streams. Streams are returned sorted by number of current viewers, in descending order. Across multiple pages of results, there may be duplicate or missing streams, as viewers join and leave streams.<br>
     * The response has a JSON payload with a data field containing an array of stream information elements and a pagination field containing information required to query for more streams.
     *
     * @apiNote <a href="https://dev.twitch.tv/docs/api/reference/#get-streams">https://dev.twitch.tv/docs/api/reference/#get-streams</a>
     */
    public static final Endpoint STREAMS = new Endpoint(Scopes.NONE, "GET", "/streams");

    /**
     * Gets metadata information about active streams playing Overwatch or Hearthstone. Streams are sorted by number of current viewers, in descending order. Across multiple pages of results, there may be duplicate or missing streams, as viewers join and leave streams.<br>
     * The response has a JSON payload with a data field containing an array of stream information elements and a pagination field containing information required to query for more streams.<br>
     * This endpoint has a global rate limit, across all callers. The limit may change over time, but the response includes informative headers:
     * <pre>
     * Ratelimit-Helixstreamsmetadata-Limit: &lt;int value&gt;
     * Ratelimit-Helixstreamsmetadata-Remaining: &lt;int value&gt;
     * </pre>
     *
     * @apiNote <a href="https://dev.twitch.tv/docs/api/reference/#get-streams-metadata">https://dev.twitch.tv/docs/api/reference/#get-streams-metadata</a>
     */
    public static final Endpoint STREAMS_METADATA = new Endpoint(Scopes.NONE, "GET", "/streams/metadata");

    /**
     * Creates a marker in the stream of a user specified by a user ID. A marker is an arbitrary point in a stream that the broadcaster wants to mark; e.g., to easily return to later. The marker is created at the current timestamp in the live broadcast when the request is processed. Markers can be created by the stream owner or editors. The user creating the marker is identified by a Bearer token.<br>
     * Markers cannot be created in some cases (an error will occur):
     * <ul>
     * <li>If the specified user’s stream is not live.
     * <li>If VOD (past broadcast) storage is not enabled for the stream.
     * <li>For premieres (live, first-viewing events that combine uploaded videos with live chat).
     * <li>For reruns (subsequent (not live) streaming of any past broadcast, including past premieres).
     * </ul>
     *
     * @apiNote <a href="https://dev.twitch.tv/docs/api/reference/#create-stream-marker">https://dev.twitch.tv/docs/api/reference/#create-stream-marker</a>
     */
    public static final Endpoint STREAMS_MARKERS_CREATE = new Endpoint(Scopes.NEW_USER_EDIT_BROADCAST, "POST", "/streams/markers");

    /**
     * Gets a list of markers for either a specified user’s most recent stream or a specified VOD/video (stream), ordered by recency. A marker is an arbitrary point in a stream that the broadcaster wants to mark; e.g., to easily return to later. The only markers returned are those created by the user identified by the Bearer token.<br>
     * The response has a JSON payload with a data field containing an array of marker information elements and a pagination field containing information required to query for more follow information.
     *
     * @apiNote <a href="https://dev.twitch.tv/docs/api/reference/#get-stream-markers">https://dev.twitch.tv/docs/api/reference/#get-stream-markers</a>
     */
    public static final Endpoint STREAMS_MARKERS_GET = new Endpoint(Scopes.NEW_USER_EDIT_BROADCAST, "GET", "/streams/markers");

    /**
     * Gets information about one or more specified Twitch users. Users are identified by optional user IDs and/or login name. If neither a user ID nor a login name is specified, the user is looked up by Bearer token.<br>
     * The response has a JSON payload with a data field containing an array of user-information elements.
     *
     * @apiNote <a href="https://dev.twitch.tv/docs/api/reference/#get-users">https://dev.twitch.tv/docs/api/reference/#get-users</a>
     */
    public static final Endpoint USERS = new Endpoint(Scopes.NEW_USER_READ_EMAIL, "GET", "/users");

    /**
     * Gets information about one or more specified Twitch users. Users are identified by optional user IDs and/or login name. If neither a user ID nor a login name is specified, the user is looked up by Bearer token.<br>
     * The response has a JSON payload with a data field containing an array of user-information elements.<br>
     * Same as users, but might not return the clients emails since the permission is not required.
     *
     * @apiNote <a href="https://dev.twitch.tv/docs/api/reference/#get-users">https://dev.twitch.tv/docs/api/reference/#get-users</a>
     */
    public static final Endpoint USERS_NO_MAIL = new Endpoint(Scopes.NONE, "GET", "/users");

    /**
     * Gets information on follow relationships between two Twitch users. Information returned is sorted in order, most recent follow first. This can return information like “who is lirik following,” “who is following lirik,” or “is user X following user Y.”<br>
     * The response has a JSON payload with a data field containing an array of follow relationship elements and a pagination field containing information required to query for more follow information.<br>
     * At minimum, from_id or to_id must be provided for a query to be valid.
     *
     * @apiNote <a href="https://dev.twitch.tv/docs/api/reference/#get-users-follows">https://dev.twitch.tv/docs/api/reference/#get-users-follows</a>
     */
    public static final Endpoint USERS_FOLLOWS = new Endpoint(Scopes.NONE, "GET", "/users/follows");

    /**
     * Updates the description of a user specified by a Bearer token.
     *
     * @apiNote <a href="https://dev.twitch.tv/docs/api/reference/#update-user">https://dev.twitch.tv/docs/api/reference/#update-user</a>
     */
    public static final Endpoint USERS_UPDATE = new Endpoint(Scopes.NEW_USER_EDIT, "PUT", "/users/follows");

    /**
     * Gets a list of all extensions (both active and inactive) for a specified user, identified by a Bearer token.<br>
     * The response has a JSON payload with a data field containing an array of user-information elements.
     *
     * @apiNote <a href="https://dev.twitch.tv/docs/api/reference/#get-user-extensions">https://dev.twitch.tv/docs/api/reference/#get-user-extensions</a>
     */
    public static final Endpoint USERS_EXTENSIONS_LIST = new Endpoint(Scopes.NEW_USER_READ_BROADCAST, "GET", "/users/extensions/list");

    /**
     * Gets information about active extensions installed by a specified user, identified by a user ID or Bearer token.
     *
     * @apiNote <a href="https://dev.twitch.tv/docs/api/reference/#get-user-active-extensions">https://dev.twitch.tv/docs/api/reference/#get-user-active-extensions</a>
     */
    public static final Endpoint USERS_EXTENSIONS_GET = new Endpoint(Scopes.NEW_USER_READ_BROADCAST, "GET", "/users/extensions");

    /**
     * Updates the activation state, extension ID, and/or version number of installed extensions for a specified user, identified by a Bearer token. If you try to activate a given extension under multiple extension types, the last write wins (and there is no guarantee of write order).
     *
     * @apiNote <a href="https://dev.twitch.tv/docs/api/reference/#update-user-extensions">https://dev.twitch.tv/docs/api/reference/#update-user-extensions</a>
     */
    public static final Endpoint USERS_EXTENSIONS_UPDATE = new Endpoint(Scopes.NEW_USER_EDIT_BROADCAST, "PUT", "/users/extensions");

    /**
     * Gets video information by video ID (one or more), user ID (one only), or game ID (one only).<br>
     * The response has a JSON payload with a data field containing an array of video elements. For lookup by user or game, pagination is available, along with several filters that can be specified as query string parameters.
     *
     * @apiNote <a href="https://dev.twitch.tv/docs/api/reference/#get-videos">https://dev.twitch.tv/docs/api/reference/#get-videos</a>
     */
    public static final Endpoint VIDEOS = new Endpoint(Scopes.NONE, "GET", "/videos");

    /**
     * Gets the Webhook subscriptions of a user identified by a Bearer token, in order of expiration.<br>
     * The response has a JSON payload with a data field containing an array of subscription elements and a pagination field containing information required to query for more subscriptions.
     *
     * @apiNote <a href="https://dev.twitch.tv/docs/api/reference/#get-webhook-subscriptions">https://dev.twitch.tv/docs/api/reference/#get-webhook-subscriptions</a>
     */
    public static final Endpoint WEBHOOKS_SUBSCRIPTIONS = new Endpoint(Scopes.NONE, "GET", "/webhooks/subscriptions");

}
