package com.itwookie.dosbot.api;

public class Scopes {

    /**
     * View analytics data for your extensions.
     */
    public static final String NEW_ANALYTICS_READ_EXTENSIONS = "analytics:read:extensions";

    /**
     * View analytics data for your games.
     */
    public static final String NEW_ANALYTICS_READ_GAMES = "analytics:read:games";

    /**
     * View Bits information for your channel.
     */
    public static final String NEW_BITS_READ = "bits:read";

    /**
     * Manage a clip object.
     */
    public static final String NEW_CLIPS_EDIT = "clips:edit";

    /**
     * Manage a user object.
     */
    public static final String NEW_USER_EDIT = "user:edit";

    /**
     * Edit your channel’s broadcast configuration, including extension configuration. (This scope implies user:read:broadcast capability.)
     */
    public static final String NEW_USER_EDIT_BROADCAST = "user:edit:broadcast";

    /**
     * View your broadcasting configuration, including extension configurations.
     */
    public static final String NEW_USER_READ_BROADCAST = "user:read:broadcast";

    /**
     * Read authorized user’s email address.
     */
    public static final String NEW_USER_READ_EMAIL = "user:read:email";

    /**
     * Perform moderation actions in a channel.
     */
    public static final String CHAT_CHANNEL_MODERATE = "channel:moderate";

    /**
     * Send live stream chat and rooms messages.
     */
    public static final String CHAT_CHAT_EDIT = "chat:edit";

    /**
     * View live stream chat and rooms messages.
     */
    public static final String CHAT_CHAT_READ = "chat:read";

    /**
     * View your whisper messages.
     */
    public static final String CHAT_WHISPERS_READ = "whispers:read";

    /**
     * Send whisper messages.
     */
    public static final String CHAT_WHISPERS_EDIT = "whispers:edit";

    /**
     * No Scope required
     */
    public static final String NONE = "";

}
