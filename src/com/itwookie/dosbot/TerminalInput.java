package com.itwookie.dosbot;

import com.itwookie.dosbot.commands.Command;
import com.itwookie.dosbot.events.client.TerminalCommandEvent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Optional;

public class TerminalInput extends Thread {

    private TwitchClient instance;

    TerminalInput(TwitchClient instance) {
        setName("TerminalInput");
        this.instance = instance;
    }

    @Override
    public void run() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (TwitchClient.isConnected()) {
            String input;
            try {
                if (br.ready())
                    input = br.readLine();
                else {
                    Thread.sleep(50);
                    continue;
                }
            } catch (InterruptedException interrupted) {
                break;
            } catch (IOException e) { //execution stopped
                break;
            }
            try {
                TerminalCommandEvent event = new TerminalCommandEvent(input);
                if (TwitchClient.getEventBus().fire(event)) {
                    Optional<Command> cmd =
                            TwitchClient.getCommands().getCommand(event.getCommandName());
                    if (cmd.isPresent())
                        cmd.get().parseArgs(instance, event);
                    else {
                        Launcher.log.err("No such command \"" + event.getCommandName() + "\"");
                    }
                }
            } catch (Exception e) { //error in command handler
                e.printStackTrace();
                Thread.yield();
            }
        }
    }
}
