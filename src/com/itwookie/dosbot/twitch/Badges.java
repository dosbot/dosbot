package com.itwookie.dosbot.twitch;

/**
 * collection of known badges by twich documentation
 */
public class Badges {
    public static final String ADMIN = "admin";
    public static final String BITS = "bits";
    public static final String BROADCASTER = "broadcaster";
    public static final String GLOBAL_MOD = "global_mod";
    public static final String MODERATOR = "moderator";
    public static final String SUBSCRIBER = "subscriber";
    public static final String STAFF = "staff";
    public static final String TURBO = "turbo";

}
