package com.itwookie.dosbot.twitch;

import java.util.HashMap;
import java.util.Map;

/**
 * class that fetches and collects cheer emotes for the channel for proper escape
 */
public class CheerRegistry {

    private Map<String, Cheer> reg = new HashMap<>();

    /**
     * add a new image-less cheer to the registry if not yet existent.
     *
     * @return the Cheer entry for this cheer name
     */
    public Cheer getOrCreate(String name) {
        name = name.toLowerCase();
        if (!reg.containsKey(name))
            reg.put(name, new Cheer(name));
        return reg.get(name);
    }

    /**
     * automatically updated cheers from online
     */
    public void fetch() {

    }

}
