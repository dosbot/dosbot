package com.itwookie.dosbot.twitch;

import com.itwookie.dosbot.TwitchClient;
import com.itwookie.dosbot.events.chat.UserConnectionEvent;
import com.itwookie.dosbot.exceptions.InsufficientPermissionException;
import com.itwookie.dosbot.irc.TagBag;
import com.itwookie.utils.ImmutableSet;
import org.intellij.lang.annotations.MagicConstant;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class Channel {

    private String name;
    private int roomID;
    private Locale broadcasterLang;
    private boolean emoteOnly;
    /**
     * if present the channel is in follower mode.
     * if > 0 determines the amount of minutes a client has to follow before they can chat
     */
    private Integer followerOnly;
    private boolean r9k;
    private int slow;
    private boolean subsOnly;

    private Map<String, User> users = new HashMap<>();

    public Channel(String channelName) {
        name = channelName;
        roomID = -1;
        broadcasterLang = null;
        emoteOnly = false;
        followerOnly = null;
        r9k = false;
        slow = 0;
        subsOnly = false;
    }

    public void update(TagBag tags) {
        roomID = s2i(tags.getOrDefault("room-id", "-1"));
        String tmp = tags.getOrDefault("broadcaster-lang", "");
        if (tmp.isEmpty()) broadcasterLang = null;
        else broadcasterLang = Locale.forLanguageTag(tmp);
        emoteOnly = s2b(tags.getOrDefault("emote-only", "0"));
        int i = s2i(tags.getOrDefault("followers-only", "-1"));
        if (i < 0) followerOnly = null;
        else followerOnly = i;
        r9k = s2b(tags.getOrDefault("r9k", "0"));
        slow = Math.max(0, s2i(tags.getOrDefault("slow", "0")));
        subsOnly = s2b(tags.getOrDefault("subs-only", "0"));
    }

    private static boolean s2b(String s) {
        if (s.isEmpty()) return false;
        return s.equals("1");
    }

    private static int s2i(String s) {
        if (s.isEmpty()) return 0;
        try {
            return Integer.valueOf(s);
        } catch (NumberFormatException e) {
            return 0;
        }
    }


    public String getName() {
        return name;
    }

    public int getRoomID() {
        return roomID;
    }

    public Optional<Locale> getBroadcasterLang() {
        return Optional.ofNullable(broadcasterLang);
    }

    public boolean isEmoteOnly() {
        return emoteOnly;
    }

    public Optional<Integer> getFollowerOnly() {
        return Optional.ofNullable(followerOnly);
    }

    public boolean isR9k() {
        return r9k;
    }

    public int getSlow() {
        return slow;
    }

    public boolean isSubsOnly() {
        return subsOnly;
    }

    /**
     * set this channel in emote only mode
     *
     * @throws InsufficientPermissionException if bot is not at least moderator
     */
    public void setEmoteOnly(boolean emoteOnly) {
        if (!TwitchClient.getSelf().isMod())
            throw new InsufficientPermissionException("Setting Emote Only mode requires at least Moderator status");
        TwitchClient.sendMessage(emoteOnly ? ".emoteonly" : ".emoteonlyoff");
    }

    /**
     * set this channel in follower only mode and update the time.
     *
     * @param minMinutes the amount of minutes a user has to have followed the channel before being able to chat.
     *                   values less than 0 will disable follow-only mode
     * @throws InsufficientPermissionException if bot is not at least moderator
     */
    public void setFollowerOnly(int minMinutes) {
        if (!TwitchClient.getSelf().isMod())
            throw new InsufficientPermissionException("Setting Follower Only mode requires at least Moderator status");
        TwitchClient.sendMessage(minMinutes >= 0 ? ".followers " + minMinutes : ".followersoff");
    }

    /**
     * enables or disables r9k mode for this channel
     *
     * @throws InsufficientPermissionException if bot is not at least moderator
     */
    public void setR9Kmode(boolean enabled) {
        if (!TwitchClient.getSelf().isMod())
            throw new InsufficientPermissionException("Toggling R9K requires at least Moderator status");
        TwitchClient.sendMessage(enabled ? ".r9kbeta" : ".r9kbetaoff");
    }

    /**
     * set this channel in slow mode and update the delay (twitch uses 120 sec as default).
     *
     * @param seconds the amount of seconds a user has to wait before being able to write the next message.
     *                values less than 1 will disable slow mode
     * @throws InsufficientPermissionException if bot is not at least moderator
     */
    public void setSlowMode(int seconds) {
        if (!TwitchClient.getSelf().isMod())
            throw new InsufficientPermissionException("Setting Slow mode requires at least Moderator status");
        TwitchClient.sendMessage(seconds > 0 ? (".slow " + seconds) : ".slowoff");
    }

    /**
     * enables or disables sub-only mode for this channel
     *
     * @throws InsufficientPermissionException if bot is not at least moderator
     */
    public void setSubOnly(boolean enabled) {
        if (!TwitchClient.getSelf().isMod())
            throw new InsufficientPermissionException("Setting Sub Only mode requires at least Moderator status");
        TwitchClient.sendMessage(enabled ? ".subscribers" : ".subscribersoff");
    }

    /**
     * host a channel, use unhost to stop hosting
     *
     * @throws InsufficientPermissionException if bot is not logged in as broadcaster
     */
    public void host(String channel, @Nullable String tagLine) {
        if (!TwitchClient.getSelf().hasBadge(Badges.BROADCASTER))
            throw new InsufficientPermissionException("Hosting a channel requires at least Broadcaster status");
        //TODO check if channel exists?
        if (tagLine != null) tagLine = tagLine.trim();
        TwitchClient.sendMessage((tagLine != null && !tagLine.isEmpty()) ? ".host " + channel + " " + tagLine : ".host " + channel);
    }

    /**
     * host a channel, use unhost to stop hosting
     *
     * @throws InsufficientPermissionException if bot is not logged in as broadcaster
     */
    public void host(String channel) {
        host(channel, null);
    }

    /**
     * stop hosting a channel
     *
     * @throws InsufficientPermissionException if bot is not logged in as broadcaster
     */
    public void unhost() {
        if (!TwitchClient.getSelf().hasBadge(Badges.BROADCASTER))
            throw new InsufficientPermissionException("Unhosting a channel requires at least Broadcaster status");
        TwitchClient.sendMessage(".unhost");
    }

    /**
     * promote a user to moderator
     *
     * @throws InsufficientPermissionException if bot is not logged in as broadcaster
     */
    public void mod(OfflineUser user) {
        if (!TwitchClient.getSelf().hasBadge(Badges.BROADCASTER))
            throw new InsufficientPermissionException("Granting moderator permissions requires at least Broadcaster status");
        TwitchClient.sendMessage(".mod " + user.getUserName());
    }

    /**
     * demote a user from moderator
     *
     * @throws InsufficientPermissionException if bot is not logged in as broadcaster
     */
    public void unmod(OfflineUser user) {
        if (!TwitchClient.getSelf().hasBadge(Badges.BROADCASTER))
            throw new InsufficientPermissionException("Revoking moderator permissions requires at least Broadcaster status");
        TwitchClient.sendMessage(".unmod " + user.getUserName());
    }

    /**
     * ban a user from chatting this channel
     *
     * @throws InsufficientPermissionException if bot is not at least moderator
     */
    public void ban(OfflineUser user, @Nullable String reason) {
        if (!TwitchClient.getSelf().isMod())
            throw new InsufficientPermissionException("Banning a user from chat requires at least Moderator status");
        if (reason != null) reason = reason.trim();
        TwitchClient.sendMessage((reason != null && !reason.isEmpty()) ? ".ban " + user.getUserName() + " " + reason : ".ban " + user.getUserName());
    }

    /**
     * ban a user from chatting this channel
     *
     * @throws InsufficientPermissionException if bot is not at least moderator
     */
    public void ban(OfflineUser user) {
        ban(user, null);
    }

    /**
     * allow a user to part-take in this chat again
     *
     * @throws InsufficientPermissionException if bot is not logged in as broadcaster
     */
    public void unban(OfflineUser user) {
        if (!TwitchClient.getSelf().isMod())
            throw new InsufficientPermissionException("Revoking a chat ban from a user requires at least Moderator status");
        TwitchClient.sendMessage(".unban " + user.getUserName());
    }

    /**
     * ban a user from chatting this channel
     *
     * @throws InsufficientPermissionException if bot is not at least moderator
     */
    public void timeout(OfflineUser user, int seconds, @Nullable String reason) {
        if (!TwitchClient.getSelf().isMod())
            throw new InsufficientPermissionException("Banning a user from chat requires at least Moderator status");
        if (seconds < 1)
            throw new IllegalArgumentException("Timeout duration has to be a positive integer");
        if (reason != null) reason = reason.trim();
        TwitchClient.sendMessage((reason != null && !reason.isEmpty()) ? ".timeout " + user.getUserName() + " " + seconds + " " + reason : ".timeout " + user.getUserName() + " " + seconds);
    }

    /**
     * ban a user from chatting this channel
     *
     * @throws InsufficientPermissionException if bot is not at least moderator
     */
    public void timeout(OfflineUser user, int seconds) {
        timeout(user, seconds, null);
    }

    /**
     * ban a user from chatting this channel
     *
     * @throws InsufficientPermissionException if bot is not at least moderator
     */
    public void timeout(OfflineUser user) {
        timeout(user, 600, null);
    }

    /**
     * allow a user to part-take in this chat again
     *
     * @throws InsufficientPermissionException if bot is not logged in as broadcaster
     */
    public void untimeout(OfflineUser user) {
        if (!TwitchClient.getSelf().isMod())
            throw new InsufficientPermissionException("Revoking a chat ban from a user requires at least Moderator status");
        TwitchClient.sendMessage(".untimeout " + user.getUserName());
    }

    /**
     * a weak attempt to clear the chat history in this channel. doesn't work for everyone
     *
     * @throws InsufficientPermissionException if bot is not logged in as broadcaster
     */
    public void clear() {
        if (!TwitchClient.getSelf().isMod())
            throw new InsufficientPermissionException("Clearing the chat history requires at least Moderator status");
        TwitchClient.sendMessage(".clear");
    }

    /**
     * tries to initiate a comercial break
     *
     * @param duration 30-180 seconds in half-minute steps, not all values might be supported for your channel.
     * @throws InsufficientPermissionException if bot is not logged in as broadcaster
     */
    public void comercial(@MagicConstant(intValues = {30, 60, 90, 120, 150, 180}) int duration) {
        if (!TwitchClient.getSelf().hasBadge(Badges.BROADCASTER))
            throw new InsufficientPermissionException("Initiating a commercial break requires at least Broadcaster status");
        if (duration % 30 != 0 || duration < 30 || duration > 180)
            throw new IllegalArgumentException("Illegal value for commercial duration");
        TwitchClient.sendMessage(".commercial " + duration);
    }

    /**
     * Don't use these as they won't throw events.<br>
     * RPL_NAMREPLY will add a few users that were already in the channel before
     * the bot joined. if these would cause a event the channel/bot would not be 100% ready yet
     * and errors might occur. For this reason and since they did not really join they should be added w/o event.
     *
     * @param issueEvent whether or not to throw a event for this user
     */
    @Deprecated
    public void __innternalAddUser(User user, boolean issueEvent) {
        if (!users.containsKey(user.getUserName())) {
            users.put(user.getUserName(), user);
            //don't fire event for self joining. the bot is always present (causes error with UserConnectionEvent.Join otherwise (initialization/event order)
            if (!user.getUserName().equalsIgnoreCase(name) && issueEvent)
                TwitchClient.getEventBus().fire(new UserConnectionEvent.Join(user));
        }
    }

    /**
     * Don't use these as they won't throw events
     */
    @Deprecated
    public void __internalRemoveUser(User user) {
        if (!user.getUserName().equals(name)) {
            TwitchClient.getEventBus().fire(new UserConnectionEvent.Part(user));
            users.remove(user.getUserName());
        }
    }

    /**
     * returns a user if the user is currently in the channel.
     * Values besides the username may be default due to the channel having 1000+ user upon joining
     */
    public Optional<User> getUser(String userName) {
        return Optional.ofNullable(users.get(userName));
    }

    /**
     * tries to find a user in this channel by login or display name
     */
    public Optional<User> findUser(@NotNull String name) {
        return users.values().stream().filter(u ->
                name.equalsIgnoreCase(u.getUserName()) ||
                        name.equalsIgnoreCase(u.getDisplayName())
        ).findAny();
    }

    public User asUser() {
        if (users.containsKey(name))
            return users.get(name);
        else {
            TagBag tags = new TagBag();
            tags.put("client-id", String.valueOf(roomID));
            tags.put("mod", "1");
            tags.put("badges", Badges.BROADCASTER + "/1");
            User user = new User(name, tags);
            __innternalAddUser(user, false);
            return user;
        }
    }

    public Collection<User> getOnline() {
        return new ImmutableSet<>(users.values());
    }

}
