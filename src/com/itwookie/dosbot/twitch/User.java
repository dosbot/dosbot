package com.itwookie.dosbot.twitch;

import com.itwookie.dosbot.TwitchClient;
import com.itwookie.dosbot.api.Endpoints;
import com.itwookie.dosbot.irc.TagBag;
import com.itwookie.dosbot.oauth2.Request;
import com.itwookie.utils.Expiring;
import org.intellij.lang.annotations.MagicConstant;
import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;

public class User extends OfflineUser implements CommandSender {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    static {
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
    }

    private Color color;
    //Maps badges and bade version for simpler checks for e.g. subscriber badge
    private Map<String, Integer> badges = new HashMap<>();
    private Set<Integer> emotes = new HashSet<>();
    private boolean mod;
    private Expiring<Optional<Date>> followDate = Expiring.expired();

    public User(@NotNull String userName, TagBag tags) {
        this.userName = userName;
        this.displayName = userName;
        this.color = null;
        this.emotes.add(0);
        this.mod = false;
        update(tags);
    }

    public void update(TagBag tags) {
        if (tags == null) return;
        if (tags.containsKey("display-name")) {
            this.displayName = tags.getOrDefault("display-name", userName);
            if (this.displayName.isEmpty()) this.displayName = this.userName;
        }
        if (tags.containsKey("color")) {
            String tmp = tags.getOrDefault("color", "");
            if (tmp.isEmpty()) color = null;
            else color = Color.decode(tmp);
        }
        if (tags.containsKey("emote-sets")) {
            emotes.clear();
            String tmp = tags.getOrDefault("emote-sets", "0");
            if (!tmp.isEmpty()) for (String part : tmp.split(","))
                emotes.add(Integer.parseInt(part));
        }
        if (tags.containsKey("badges")) {
            badges.clear();
            String tmp = tags.getOrDefault("badges", "");
            if (!tmp.isEmpty()) for (String part : tmp.split(",")) {
                String[] kv = part.split("/");
                badges.put(kv[0], Integer.parseInt(kv[1]));
            }
        }
        if (tags.containsKey("mod"))
            mod = tags.getOrDefault("mod", "0").equals("1");
        if (userID <= 0 && tags.containsKey("user-id")) { //should not update
            String tmp = tags.getOrDefault("user-id", "0");
            if (!tmp.isEmpty()) userID = Integer.valueOf(tmp);
        }
    }

    /**
     * Updates user based on UserNotice, this can e.g. add or increase the subscriber badge level
     */
    public void notify(TagBag tags) {
        update(tags);
    }

    public Optional<Color> getColor() {
        return Optional.ofNullable(color);
    }

    public boolean isMod() {
        return mod;
    }

    /**
     * @return IDs of emote sets
     */
    public Set<Integer> getEmotes() {
        return emotes;
    }


    public boolean hasBadge(@MagicConstant(valuesFromClass = Badges.class) String badge) {
        return badges.containsKey(badge);
    }

    public Set<String> getBadges() {
        return badges.keySet();
    }

    /**
     * @return the badge level or null if the badge is not listed
     */
    public Integer getBadgeLevel(@MagicConstant(valuesFromClass = Badges.class) String badge) {
        return badges.get(badge);
    }

    /**
     * This value expires every 5 minutes, and has to be renewed.
     * In case the value has expired this method blocks until the API responds with a new value.
     *
     * @return whether or not this user is following the channel.
     */
    public Boolean isFollower() {
        try {
            return queryFollowDate().get().isPresent();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * This method uses isFollower() to update the follow value if necessary, thus it's blocking.
     *
     * @return the exact time since the user follows
     */
    public Optional<Date> getFollowDate() {
        if (isFollower()) {
            return followDate.get();
        } else {
            return Optional.empty();
        }
    }

    /**
     * Get a future completable, that sometimes will complete with information about
     * wether or not this user follows.
     */
    public CompletableFuture<Optional<Date>> queryFollowDate() {
        CompletableFuture<Optional<Date>> result = new CompletableFuture<>();
        int channelUID = TwitchClient.getChannel().asUser().userID;
        int myUID = getUserID();
        if (channelUID <= 0 || myUID <= 0) {
            result.completeExceptionally(new IllegalStateException("Some userid is missing"));
            return result;
        }
        if (followDate.isAlive())
            result.complete(followDate.get());
        else {
            try {
                Request req = TwitchClient.getTwitchAPI().build(Endpoints.USERS_FOLLOWS);
                req.setParameter("from_id", String.valueOf(myUID));
                req.setParameter("to_id", String.valueOf(channelUID));
                TwitchClient.getTwitchAPI().send(req)
                        .thenAccept((json) -> {
                            if (json.getInt("total") < 1) {
                                followDate = Expiring.expireIn(Optional.empty(), 5 * 60 * 1000);
                            } else {
                                String date = json.getJSONArray("data")
                                        .getJSONObject(0)
                                        .getString("followed_at");
                                try {
                                    followDate = Expiring.expireIn(Optional.of(dateFormat.parse(date)), 5 * 60 * 1000);
                                } catch (ParseException e) {
                                    followDate = Expiring.expireIn(Optional.empty(), 5 * 60 * 1000);
                                }
                                result.complete(followDate.getAnyways());
                            }
                        });
            } catch (Exception e) {
                result.completeExceptionally(e);
            }
        }
        return result;
    }

    /**
     * shorthand for hasBadge(Badges.SUBSCRIBER)
     */
    public boolean isSubscriber() {
        return hasBadge(Badges.SUBSCRIBER);
    }

}
