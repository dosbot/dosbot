package com.itwookie.dosbot.twitch;

import com.itwookie.dosbot.TwitchClient;
import com.itwookie.dosbot.exceptions.InsufficientPermissionException;
import com.itwookie.dosbot.irc.TagBag;

import java.util.Optional;

/**
 * offline users allow for all interactions that do not require the user to bbe present in chat.<br>
 * this limits interaction to a subset, that does not require any permission on the side of this offline user.<br>
 * the userID for offline users might not be present, thus preventing certain operations
 */
public class OfflineUser {
    OfflineUser() {
    }

    /**
     * @param login       the pre-extracted login from tags; required as tags may contain multiple users
     * @param displayName the pre-extracted displayName from tags; required as tags may contain multiple users
     * @param userID      the pre-extracted userID from tags; required as tags may contain multiple users
     */
    public OfflineUser(String login, String displayName, int userID) {
        this.userName = login;
        this.displayName = displayName;
        this.userID = userID;
    }

    /**
     * uses the default keys login, display-name and user-id from tags
     */
    public OfflineUser(TagBag tags) {
        this.userName = tags.getOrDefault("login", "");
        this.displayName = tags.getOrDefault("display-name", "");
        try {
            this.userID = Integer.valueOf(tags.getOrDefault("user-id", "-1"));
        } catch (NumberFormatException ignore) {
            this.userID = -1;
        }
    }

    protected String userName;
    protected String displayName;
    protected int userID;

    public String getUserName() {
        return userName;
    }

    public String getDisplayName() {
        return displayName;
    }

    /**
     * ONLY valid if > 0
     */
    public int getUserID() {
        return userID;
    }

    public Optional<User> getOnline() {
        return TwitchClient.getChannel().getUser(userName);
    }

    /**
     * promotes this user to moderator.<br>
     * shorthand for channel.mod(user)
     *
     * @throws InsufficientPermissionException if bot is not logged in as broadcaster
     */
    public void promote() {
        TwitchClient.getChannel().mod(this);
    }

    /**
     * demotes this user from moderator.<br>
     * shorthand for channel.unmod(user)
     *
     * @throws InsufficientPermissionException if bot is not logged in as broadcaster
     */
    public void demote() {
        TwitchClient.getChannel().unmod(this);
    }

    /**
     * temporarily bans a user from chatting.<br>
     * shorthand for channel.timeout(user)
     *
     * @throws InsufficientPermissionException if bot is not at least moderator
     */
    public void timeout() {
        TwitchClient.getChannel().timeout(this, 600, null);
    }

    /**
     * temporarily bans a user from chatting.<br>
     * shorthand for channel.timeout(user, duration)
     *
     * @throws InsufficientPermissionException if bot is not at least moderator
     */
    public void timeout(int duration) {
        TwitchClient.getChannel().timeout(this, duration, null);
    }

    /**
     * temporarily bans a user from chatting.<br>
     * shorthand for channel.timeout(user, duration, reason)
     *
     * @throws InsufficientPermissionException if bot is not at least moderator
     */
    public void timeout(int duration, String reason) {
        TwitchClient.getChannel().timeout(this, duration, reason);
    }

    /**
     * revokes a timeout.<br>
     * shorthand for channel.untimeout(user)
     *
     * @throws InsufficientPermissionException if bot is not at least moderator
     */
    public void untimeout() {
        TwitchClient.getChannel().untimeout(this);
    }

    /**
     * ban a user from chatting.<br>
     * shorthand for channel.ban(user)
     *
     * @throws InsufficientPermissionException if bot is not at least moderator
     */
    public void ban() {
        TwitchClient.getChannel().ban(this, null);
    }

    /**
     * ban a user from chatting.<br>
     * shorthand for channel.ban(user, reason)
     *
     * @throws InsufficientPermissionException if bot is not at least moderator
     */
    public void ban(String reason) {
        TwitchClient.getChannel().ban(this, reason);
    }

    /**
     * revokes a ban.<br>
     * shorthand for channel.unban(user)
     *
     * @throws InsufficientPermissionException if bot is not at least moderator
     */
    public void unban() {
        TwitchClient.getChannel().unban(this);
    }
}
