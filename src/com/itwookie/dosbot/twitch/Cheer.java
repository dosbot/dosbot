package com.itwookie.dosbot.twitch;

import java.net.URL;
import java.util.Optional;

public class Cheer {
    public enum Theme {Light, Dark}

    public enum Type {Animated, Static}

    public enum Color {
        Red(10000), Blue(5000), Green(1000), Purple(100), Gray(1);
        private final int minValue;

        Color(int minValue) {
            this.minValue = minValue;
        }

        public int getMinValue() {
            return minValue;
        }

        public static Color forValue(int value) {
            for (Color c : values()) {
                if (value > c.minValue) return c;
            }
            return Gray;
        }
    }


    private String name;
    private URL[][][][] images = new URL
            [Theme.values().length]
            [Type.values().length]
            [Color.values().length]
            [4]; //size

    public Cheer(String emoteName) {
        this.name = emoteName;
    }

    public void addVariantImage(Theme theme, Type type, Color color, int size, URL imgUrl) {
        if (size < 1 || size > 4) throw new IllegalArgumentException("Illegal size");
        images[theme.ordinal()][type.ordinal()][color.ordinal()][size - 1] = imgUrl;
    }

    public Optional<URL> getVariantImage(Theme theme, Type type, Color color, int size) {
        if (size < 1 || size > 4) throw new IllegalArgumentException("Illegal size");
        try {
            return Optional.ofNullable(images[theme.ordinal()][type.ordinal()][color.ordinal()][size - 1]);
        } catch (ArrayIndexOutOfBoundsException ex) {
            return Optional.empty();
        }
    }

    public String getName() {
        return name;
    }
}
