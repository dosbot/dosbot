package com.itwookie.dosbot.eventBus;

import com.itwookie.dosbot.events.CancelableEvent;
import com.itwookie.dosbot.events.Event;
import com.itwookie.utils.Priorized;
import com.itwookie.utils.Priorizer;

import java.io.Closeable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class EventBus extends Thread implements Closeable {

    //region Async Event Hanlding
//    private LinkedList<Event> queue = new LinkedList<>();
//    private final Object queueMutex = new Object();
//    public void fire(Event event) {
//        synchronized (queueMutex) {
//            queue.add(event);
//        }
//    }
//    @Override
//        public void run() {
//            while (running) {
//                Event e;
//                synchronized (queueMutex) {
//                    e = queue.isEmpty() ? null : queue.pop();
//                }
//                if (e == null) {
//                    wait(50);
//                } else {
//                    consumeEvent(e);
//                    Thread.yield();
//                }
//            }
//        }
    //endregion

    //region Sync Event Handling

    public EventBus() {
        setName("Event Bus");
    }

    /**
     * @return true if the event passed all listeners without being cancelled
     */
    public boolean fire(Event event) {
        consumeEvent(event);
        // 'continue if not cancelled'
        return (!(event instanceof CancelableEvent)) || !((CancelableEvent) event).isCancelled();
    }

    //template run
    @Override
    public void run() {
        while (running) wait(100);
    }
    ///endregion

    private boolean running = true;

    @Override
    public void close() {
        running = false;
    }

    Map<Class<? extends Event>, Set<Priorized<Consumer<? extends Event>>>> subscribers = new HashMap<>();

    private <T extends Event> Set<Consumer<? extends Event>> getSubscribers(T event) {
        Class<T> eventClass = (Class<T>) event.getClass();
        Set<Class<? extends Event>> setKeys = new HashSet<>();
        SortedSet<Priorized<Consumer<? extends Event>>> consumers = new TreeSet<>(new Priorizer());
        for (Class<? extends Event> k : subscribers.keySet()) {
            if (k.isAssignableFrom(eventClass)) {
                setKeys.add(k);
            }
        }
        for (Class<? extends Event> key : setKeys) {
            consumers.addAll(subscribers.get(key));
        }
        return consumers
                .stream()
                .map(Priorized::get)
                .collect(Collectors.toSet());
    }

    public <T extends Event> void subscribe(Class<T> event, Consumer<T> subscriber) {
        Objects.requireNonNull(event);
        SortedSet<Priorized<Consumer<? extends Event>>> registered = (SortedSet) subscribers.get(event);
        if (registered == null) registered = new TreeSet<>(new Priorizer());
        registered.add(new Priorized<>(subscriber));
        subscribers.put(event, registered);
    }

    public void unsubscribeAll() {
        subscribers.clear();
    }

    //region Reflective Subscription
    @FunctionalInterface
    private interface TryConsumer<C, E extends Exception> {
        void accept(C c) throws E;
    }

    private <C> Consumer<C> tryConsume(TryConsumer<C, Exception> consumer) {
        return c -> {
            try {
                consumer.accept(c);
            } catch (Exception e) {
                throw (e instanceof RuntimeException) ? (RuntimeException) e : new RuntimeException(e);
            }
        };
    }

    /**
     * @param owner the instance object holding subscriber, if subscriber is not static
     */
    public <T extends Event> void subscribe(Object owner, Method subscriber) {
        Subscribe eventMeta = subscriber.getDeclaredAnnotation(Subscribe.class);
        Parameter[] params = subscriber.getParameters();
        if (eventMeta == null)
            return;
        if (params.length != 1)
            throw new IllegalArgumentException("The subscriber may only have one argument - The event!");
        if (!Event.class.isAssignableFrom(params[0].getType()))
            throw new IllegalArgumentException("The subscriber " + subscriber.getName() + " does not listen to Events?");
        if ((subscriber.getModifiers() & Modifier.STATIC) == 0 && owner == null)
            throw new IllegalArgumentException("Static methods require the owner object for invocation");
        subscribe((Class<T>) params[0].getType(), tryConsume((e) -> subscriber.invoke(owner, e)));
    }

    /**
     * subscribe all methods for this instance
     */
    public <T> void findSubscribersFor(T t) {
        Class<T> ct = (Class<T>) t.getClass();
        for (Method m : ct.getDeclaredMethods()) {
            if ((m.getModifiers() & Modifier.STATIC) == 0)
                subscribe(t, m);
            else
                subscribe(null, m);
        }
    }

    /**
     * subscribe static methods for this class
     */
    public <T> void findSubscribersFor(Class<T> ct) {
        for (Method m : ct.getDeclaredMethods()) {
            if ((m.getModifiers() & Modifier.STATIC) != 0)
                subscribe(null, m);
        }
    }
    //endregion

    private <T extends Event> void consumeEvent(T event) {
        Set<Consumer<? extends Event>> subs = getSubscribers(event);
        for (Consumer<? extends Event> sub : subs) {
            try {
                ((Consumer<? super T>) sub).accept(event);
            } catch (RuntimeException e) {
                Throwable cause = e.getCause();
                if (cause == null) {
                    e.printStackTrace(); //internal error
                } else if (cause instanceof InvocationTargetException) {
                    Throwable innerCause = cause.getCause(); //get actual cause;
                    if (innerCause == null) {
                        cause.printStackTrace(); //unknown ite
                    } else {
                        innerCause.printStackTrace(); //probably a plugin error, we don't want to crash the event bus
                    }
                } else {
                    cause.printStackTrace(); //unknown unpacked error
                }
            }
        }
    }

    void wait(int ms) {
        try {
            Thread.sleep(ms);
        } catch (Exception ignore) {
        }
    }
}
