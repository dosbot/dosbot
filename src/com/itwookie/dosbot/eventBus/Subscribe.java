package com.itwookie.dosbot.eventBus;

import com.itwookie.utils.Priority;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static com.itwookie.utils.Priority.Normal;


/**
 * this is a method annotation for event subscribers.<br>
 * All methods handling events shall look like this:<br>
 * <pre>
 * &#x40;Subscribe
 * public void listernName(&lt;? extends Event&gt; event) {
 *     //access event for further information
 * }
 * </pre>
 * These methods can be automatically added as subscribers to
 * the event bus by calling <code>eventBus.findSubscribersFor(HoldingObject)</code>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Subscribe {
    Priority priority = Normal;
}
