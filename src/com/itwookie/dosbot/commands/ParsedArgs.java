package com.itwookie.dosbot.commands;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;

public class ParsedArgs {

    ParsedArgs() {
    }

    /**
     * resolution
     */
    Map<String, Optional<?>> parsedArgs = new HashMap<>();

    //region Value Resolving

    /**
     * @return a optional wrapping the argument, that's empty if the OptionalArgument was not specified
     * @throws NoSuchElementException if there was no argument with that name defined
     * @throws ClassCastException     if you specified the wrong type retrieving this argument
     */
    public <T> Optional<T> get(String argumentName) {
        if (!parsedArgs.containsKey(argumentName))
            throw new NoSuchElementException("Argument unspecified: " + argumentName);

        Optional<?> mysterious = parsedArgs.get(argumentName);
        if (!mysterious.isPresent()) return Optional.empty();
        //throws CCE if mismatch
        T value = (T) mysterious.get();
        return Optional.of(value);
    }
    //endregion
}
