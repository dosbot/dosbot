package com.itwookie.dosbot.commands;

import com.itwookie.dosbot.TwitchClient;

import java.util.Optional;

public class IntegerArgument implements GenericArgument<Integer> {

    private String argName;

    public IntegerArgument(String argName) {
        this.argName = argName;
    }

    @Override
    public String getName() {
        return argName;
    }

    @Override
    public ArgumentParser<Integer> getParser(TwitchClient client) {
        return new ArgumentParser<>() {
            Integer value = null;

            @Override
            public String parse(String raw) {
                int i = raw.indexOf(' ');
                String arg = i >= 0 ? raw.substring(0, i) : raw;

                value = Integer.parseInt(arg);

                return i < 0 ? "" : raw.substring(i + 1);
            }

            @Override
            public Optional<Integer> value() {
                return Optional.ofNullable(value);
            }
        };
    }

}
