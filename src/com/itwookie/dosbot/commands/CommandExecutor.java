package com.itwookie.dosbot.commands;

import com.itwookie.dosbot.twitch.CommandSender;

@FunctionalInterface
public interface CommandExecutor {

    /**
     * actually handle the command with the given arguments. you can extract the arguments by name from the args object.
     * the person in chat will passed in as caller. if bot is running in terminal mode, the terminal can also issue commands.
     * since these calls are not performed by a chat user the caller arguments will be a terminal inn those cases.
     * You can check that with <code>if (caller instanceof User)</code> or <code>if (caller instanceof Terminal)</code>
     *
     * @param caller The source for invoking this command
     * @param args   The parsed argument bag
     */
    void run(CommandSender caller, ParsedArgs args);

}
