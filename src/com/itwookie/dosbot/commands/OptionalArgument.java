package com.itwookie.dosbot.commands;

import com.itwookie.dosbot.TwitchClient;

import java.util.Optional;

public class OptionalArgument<T> implements GenericArgument<T> {

    private GenericArgument<T> arg;

    public OptionalArgument(GenericArgument<T> requiredArgument) {
        arg = requiredArgument;
    }

    @Override
    public String getName() {
        return arg.getName();
    }

    @Override
    public ArgumentParser<T> getParser(TwitchClient client) {
        return new ArgumentParser<>() {
            private ArgumentParser<T> internalParser = arg.getParser(client);

            /** consume a part of the raw argument string to convert it into a T object.
             * This method will not throw in order to allow optional arguments.
             * @return the argument string after the argument and argument separator if a argument T could be parsed, the previous string otherwise */
            @Override
            public String parse(String raw) {
                try {
                    return internalParser.parse(raw);
                } catch (ArgumentParseException ignore) {
                    return raw;
                }
            }

            /** @return the converted value for this argument or empty in case parseOptional missed */
            @Override
            public Optional<T> value() {
                return internalParser.value();
            }
        };
    }
}
