package com.itwookie.dosbot.commands;

import com.itwookie.dosbot.TwitchClient;

public interface GenericArgument<T> {
    String getName();

    /**
     * @param client in case the argument depends in any way on data of the client
     */
    ArgumentParser<T> getParser(TwitchClient client);
}
