package com.itwookie.dosbot.commands;

import com.itwookie.dosbot.TwitchClient;

import java.util.Optional;

public class StringArgument implements GenericArgument<String> {

    private String argName;

    public StringArgument(String argName) {
        this.argName = argName;
    }

    @Override
    public String getName() {
        return argName;
    }

    @Override
    public ArgumentParser<String> getParser(TwitchClient client) {
        return new ArgumentParser<>() {
            String value = null;

            @Override
            public String parse(String raw) {
                StringBuilder sb = new StringBuilder();
                boolean quoted = (raw.length() > 1 && (raw.charAt(0) == '"' || raw.charAt(0) == '\''));
                int l = quoted ? 1 : 0;
                for (; l < raw.length(); l++) {
                    char c = raw.charAt(l);
                    if (c == ' ' && !quoted) break;
                    if (quoted) {
                        //l++ => l post quoted so l+1 skips space
                        if (c == raw.charAt(0)) {
                            l++;
                            break;
                        } else if (l + 1 == raw.length())
                            throw new ArgumentParseException("Quotes never close!");
                    }
                    sb.append(c);
                }
                value = sb.toString();
                if (l >= raw.length()) {
                    return "";
                } else {
                    return raw.substring(l + 1);
                }
            }

            @Override
            public Optional<String> value() {
                return Optional.ofNullable(value);
            }
        };
    }

}
