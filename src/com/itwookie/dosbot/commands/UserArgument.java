package com.itwookie.dosbot.commands;

import com.itwookie.dosbot.TwitchClient;
import com.itwookie.dosbot.twitch.User;

import java.util.Optional;

public class UserArgument implements GenericArgument<User> {

    private String argName;

    public UserArgument(String argName) {
        this.argName = argName;
    }

    @Override
    public String getName() {
        return argName;
    }

    @Override
    public ArgumentParser<User> getParser(TwitchClient client) {
        return new ArgumentParser<>() {
            TwitchClient twitch = client;
            User value = null;

            @Override
            public String parse(String raw) {
                int i = raw.indexOf(' ');
                String arg = i >= 0 ? raw.substring(0, i) : raw;

                //Mentions are formatted @Username, but Username shall work as well
                String username = arg.charAt(0) == '@' ? arg.substring(1) : arg;
                value = TwitchClient.getChannel().findUser(username).orElseThrow(() -> new IllegalArgumentException("User not in chat"));

                return i < 0 ? "" : raw.substring(i + 1);
            }

            @Override
            public Optional<User> value() {
                return Optional.ofNullable(value);
            }
        };
    }

}
