package com.itwookie.dosbot.commands;

import com.itwookie.dosbot.TwitchClient;
import com.itwookie.dosbot.events.chat.UserCommandEvent;
import com.itwookie.dosbot.events.client.TerminalCommandEvent;

public class Command {
    private String name;
    private Arguments arguments;
    private CommandExecutor executor;

    //region Builder
    private Command() {

    }

    public static class Builder {
        private Command cmd;

        private Builder(String name) {
            cmd = new Command();
            cmd.name = name;
            cmd.arguments = Arguments.NONE;
            cmd.executor = null;
        }

        public Builder args(Arguments arguments) {
            cmd.arguments = arguments;
            return Builder.this;
        }

        public Builder execute(CommandExecutor executor) {
            cmd.executor = executor;
            return Builder.this;
        }

        //requires Permission
        //requires Rank
        public Command build() {
            return cmd;
        }
    }

    public static Builder builder(String commandName) {
        return new Builder(commandName);
    }
    //endregion

    public void parseArgs(TwitchClient client, UserCommandEvent cmd) {
        if (executor == null) return;
        ParsedArgs args = arguments.parse(client, cmd.getArumentString());
        executor.run(cmd.getSender(), args);
    }

    public void parseArgs(TwitchClient client, TerminalCommandEvent cmd) {
        if (executor == null) return;
        ParsedArgs args = arguments.parse(client, cmd.getArumentString());
        executor.run(TwitchClient.getSender(), args);
    }

    public String getName() {
        return name;
    }
}
