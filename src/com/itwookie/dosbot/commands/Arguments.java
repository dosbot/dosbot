package com.itwookie.dosbot.commands;

import com.itwookie.dosbot.TwitchClient;

import java.util.LinkedList;
import java.util.List;

public class Arguments {

    /**
     * definition
     */
    private List<GenericArgument> argParsers;

    //region Builder
    private Arguments() {
        argParsers = new LinkedList<>();
    }

    public static class Builder {
        private Arguments args;

        private Builder() {
            args = new Arguments();
        }

        public Builder next(GenericArgument<?> argument) {
            args.argParsers.add(argument);
            return Builder.this;
        }

        public Arguments build() {
            return args;
        }
    }

    public static Builder builder() {
        return new Builder();
    }
    //endregion

    static final Arguments NONE = new Builder().build();

    ParsedArgs parse(TwitchClient client, String argString) {
        ParsedArgs result = new ParsedArgs();
        String workingCopy = argString;
        for (GenericArgument<?> arg : argParsers) {
            ArgumentParser<?> parser = arg.getParser(client);
            workingCopy = parser.parse(workingCopy);
            result.parsedArgs.put(arg.getName(), parser.value());
        }
        return result;
    }
}
