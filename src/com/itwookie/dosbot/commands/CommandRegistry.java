package com.itwookie.dosbot.commands;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class CommandRegistry {

    private Map<String, Command> registeredCommands = new HashMap<>();

    public void register(Command command) {
        if (registeredCommands.containsKey(command.getName().toLowerCase()))
            throw new IllegalArgumentException("AbstractCommand " + command.getName().toLowerCase() + "already registered!");
        registeredCommands.put(command.getName().toLowerCase(), command);
    }

    public Optional<Command> getCommand(String name) {
        return Optional.ofNullable(registeredCommands.get(name.toLowerCase()));
    }
}
