package com.itwookie.dosbot.commands;

import java.util.Optional;

/**
 * live instance of argument parsing
 */
public interface ArgumentParser<T> {

    /**
     * consume a part of the raw argument string to convert it into a T object.
     *
     * @return the argument string after the argument and argument separator if a argument T could be parsed
     * @throws ArgumentParseException if the argument could not be parsed
     */
    String parse(String raw);

    /**
     * @return the converted value for this argument or null if not yet called/failed
     */
    Optional<T> value();

}
