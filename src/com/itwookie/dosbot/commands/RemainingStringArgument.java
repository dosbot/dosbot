package com.itwookie.dosbot.commands;

import com.itwookie.dosbot.TwitchClient;

import java.util.Optional;

public class RemainingStringArgument implements GenericArgument<String> {

    private String argName;

    public RemainingStringArgument(String argName) {
        this.argName = argName;
    }

    @Override
    public String getName() {
        return argName;
    }

    @Override
    public ArgumentParser<String> getParser(TwitchClient client) {
        return new ArgumentParser<>() {
            String value = null;

            @Override
            public String parse(String raw) {
                if (!raw.isEmpty()) value = raw;
                return "";
            }

            @Override
            public Optional<String> value() {
                return Optional.ofNullable(value);
            }
        };
    }

}
